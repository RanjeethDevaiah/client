// Include gulp
var gulp = require('gulp');

var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');


gulp.task('scripts',function() {

    return gulp.src('./src/external/*.js')
    .pipe(concat('intermediate.js'))
    .pipe(gulp.dest('lib'))
    .pipe(rename('lib.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('www/js'));
});

gulp.task('watch', function(){
    gulp.watch('./src/external/*.js',['scripts']);

});


gulp.task('default',['scripts','watch']);