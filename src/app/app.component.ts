import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SQLite } from '@ionic-native/sqlite';
import { PageNavigationProvider } from './providers/page-navigation.provider';
import { SqliteDbMigrations } from './providers/sqlit-db-migrations';
import { BackgroundMode } from '@ionic-native/background-mode';
import { AlertController, ToastController } from 'ionic-angular';
import { SettingsProvider } from './providers/settings.provider';
import { StateProvider } from './providers/app-state.provider';
import { ActionTransfer } from './providers/action-transfer.provider';
import 'rxjs/Rx';

declare var navigator: any;

@Component({
  templateUrl: 'app.component.html'
})

export class AppComponent {

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    private settings: SettingsProvider,
    private alertCtrl: AlertController,
    private toastCtrl: ToastController,
    private appState: StateProvider,
    private action: ActionTransfer,
    private pageNav: PageNavigationProvider,
    private backgroundMode:BackgroundMode) {
    platform.ready().then(() => {    
      this.backgroundMode.enable();   

      if (platform.is('ios')){
        statusBar.overlaysWebView(true);
        statusBar.styleLightContent();
      }
     
      splashScreen.hide();

      this.checkValidSettings();
      this.setNetworkAlerts();

      let dbSetup = new SqliteDbMigrations(new SQLite());

      dbSetup.intitialize()
        .then(() => {
          return dbSetup.upgrade();
        })
        .then(() => {
          this.action.componentsInitialized({
            db: dbSetup.getDb()
          });
          console.log("completed db creation.");
        })
        .catch(error => {
          console.error(error);
        });
    });
  }

  setNetworkAlerts(): void {

    this.settings.subscribeNetworkOnConnect()
      .subscribe(data => {
        let toast = this.toastCtrl.create({
          message: 'Network connected.',
          duration: 3000,
          position: 'middle'
        });
        toast.present();
      },
      error => {
        console.log(error);
      });

    this.settings.subscribeNetworkDisConnect()
      .subscribe(data => {
        let toast = this.toastCtrl.create({
          message: 'Network disconnected.',
          duration: 3000,
          position: 'middle'
        });
        toast.present();
      },
      error => {
        console.log(error);
      });
  }

  private closeApp(): void {
    if (navigator && navigator.app)
      navigator.app.exitApp();
  }

  checkValidSettings(): void {

    this.settings.checkIfLocationAuthorized()
      .then(authorized => {
        this.enableLocation();
      }).catch(error => {
        this.alertNotAuthorized(error);
      });
  }

  alertNotAuthorized(error): void {
    let alert = this.alertCtrl.create({
      title: 'Authorize GPS',
      message: error + '. Do you want to Authorize GPS ?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('User decides not to enable GPS');
            this.closeApp();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.settings.requestLocationAuthorization()
              .then(success => {
                this.enableLocation();
              });
          }
        }
      ]
    });
    alert.present();
  }

  enableLocation(): void {
    this.settings.checkIfLocationEnabled()
      .then(success => {
        console.log('GPS is enabled. No issues with the location settings, Status is ' + success);
      }).catch(error => {
        this.alertNotEnabled(error);
      });
  }

  alertNotEnabled(error): void {

    let alert = this.alertCtrl.create({
      title: 'Enable GPS',
      message: error + '. Do you want to enable GPS ?',
      buttons: [
        {
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('User decides not to enable GPS');
            this.closeApp();
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.settings.switchToGPS();
          }
        }
      ]
    });
    alert.present();
  }
}
