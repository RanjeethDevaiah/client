import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AppComponent} from './app.component';
import { CallNumber } from '@ionic-native/call-number';
import { AppRoutingComponent } from './modules/app-routing/app-routing.module'
import { RootComponent } from './components/root/root.component';
import { DonutComponent } from './components/donut/donut.component';
import { SearchMapComponent } from './components/search-map/search-map.component';
import { TimerComponent } from './components/timer/timer.component';
import { ExitPageComponent } from './components/exit-page/exit-page.component';
import { PageTitleComponent } from './components/page-title/page-title.component';
import { RideRequestComponent } from './components/ride-request/ride-request.component';
import { DriverDetailsComponent } from './components/driver-details/driver-details.component';
import { SeekRideComponent } from './components/seek-ride/seek-ride.component';
import { PageLoadComponent } from './components/page-load/page-load.component';
import { GenderComponent } from './components/gender/gender.component';
import { PhoneComponent } from './components/phone/phone.component';
import { SsnComponent } from './components/ssn/ssn.component';
import { WaitActionComponent } from './components/wait-action/wait-action.component';
import { AutoCompleteComponent } from './components/auto-suggest/autoSuggest.component';
import { MainPageComponent } from './pages/main/main-page.component';
import { BookingPageComponent } from './pages/main/booking/booking-page.component';
import { TripCompletePageComponent } from './pages/shared/trip-complete/trip-complete-page.component';
import { DrivePageComponent } from './pages/main/drive/drive-page.component';
import { RegistrationPageComponent } from './pages/shared/registration/registration-page.component';
import { ClientPaymentPageComponent } from './pages/shared/payment-client/client-payment-page.component';
import { FavPageComponent } from './pages/shared/fav/fav-page.component';
import { AccountPaymentPageComponent } from './pages/shared/payment-account/account-payment-page.component'
import { TermsPageComponent } from './pages/shared/terms/terms-page.component';
import { TransactionPageComponent } from './pages/shared/transactions/transactions-page.component';
import { AuthCodePageComponent } from './pages/shared/auth-code/auth-code-page.component';

import { LoginPageComponent } from './pages/login/login-page.component';

import { ScriptLoaderService } from './providers/script-loader.service';
import { LocationTracker } from './providers/location-tracker.provider';
import { MapService } from './providers/map.provider';
import { ActionTransfer } from './providers/action-transfer.provider';
import { NavigationService } from './providers/navigation.provider';
import { XChangeProvider } from './providers/xchange.provider';
import { SettingsProvider } from './providers/settings.provider';
import { EventProvider } from './providers/event.provider';
import { PageNavigationProvider } from './providers/page-navigation.provider';
import { StateProvider } from './providers/app-state.provider';
import { HttpClientProvider } from './providers/httpClient.provider'
import { DelayCalculation } from "./providers/delay-calculation";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';
import { LaunchNavigator } from '@ionic-native/launch-navigator';
import { SQLite } from '@ionic-native/sqlite';
import { Geolocation } from '@ionic-native/geolocation';;
import { BackgroundGeolocation } from '@ionic-native/background-geolocation';
import { Keyboard } from '@ionic-native/keyboard';
import { Dialogs } from "@ionic-native/dialogs";
import { Camera } from '@ionic-native/camera';
import { BackgroundMode } from '@ionic-native/background-mode';

@NgModule({
  declarations: [
    AppComponent,
    RootComponent,
    MainPageComponent,
    BookingPageComponent,
    DonutComponent,
    SearchMapComponent,
    DrivePageComponent,
    LoginPageComponent,   
    TimerComponent,
    RegistrationPageComponent,
    ExitPageComponent,
    PageTitleComponent,
    ClientPaymentPageComponent,
    AccountPaymentPageComponent,
    FavPageComponent,
    RideRequestComponent,
    DriverDetailsComponent,
    SeekRideComponent,
    PageLoadComponent,
    GenderComponent,
    PhoneComponent,
    TripCompletePageComponent,
    SsnComponent,
    WaitActionComponent,
    TermsPageComponent,
    TransactionPageComponent,
    AutoCompleteComponent,
    AuthCodePageComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(AppComponent),
    AppRoutingComponent
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    AppComponent,
    RootComponent,
    MainPageComponent,
    BookingPageComponent,
    DonutComponent,
    SearchMapComponent,
    DrivePageComponent,
    LoginPageComponent,   
    TimerComponent,
    RegistrationPageComponent,
    ExitPageComponent,
    PageTitleComponent,
    ClientPaymentPageComponent,
    AccountPaymentPageComponent,
    FavPageComponent,
    RideRequestComponent,
    DriverDetailsComponent,
    SeekRideComponent,
    PageLoadComponent,
    GenderComponent,
    PhoneComponent,
    TripCompletePageComponent,
    SsnComponent,
    WaitActionComponent,
    TermsPageComponent,
    TransactionPageComponent,
    AutoCompleteComponent,
    AuthCodePageComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ScriptLoaderService,
    LocationTracker,
    MapService,
    ActionTransfer,
    NavigationService,
    XChangeProvider,
    SettingsProvider,
    EventProvider,
    PageNavigationProvider,
    StateProvider,
    Diagnostic,
    Network,
    LaunchNavigator,
    SQLite,    
    BackgroundGeolocation,
    Geolocation,
    Keyboard,
    HttpClientProvider,
    DelayCalculation,
    Dialogs,
    Camera,
    BackgroundMode,
    CallNumber
  ]
})
export class AppModule {}
