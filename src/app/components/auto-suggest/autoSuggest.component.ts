import { Component, Input, forwardRef, ElementRef, AfterViewInit, ChangeDetectorRef, AfterViewChecked } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    templateUrl: "autoSuggest.component.html",
    selector: "auto-suggest",
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => AutoCompleteComponent),
        multi: true
    }]
})

export class AutoCompleteComponent implements ControlValueAccessor, AfterViewInit, AfterViewChecked {
    val: string = "";
    labelTxt: string = "";
    private propagateChange = (_: any) => { };
    private inputElement: HTMLInputElement;
    private ionItem: HTMLElement;
    showSuggest: boolean = false;
    suggestData: string[] = [];
    dataSet: string[] = [];
    constructor(private ref: ElementRef, private changeRef: ChangeDetectorRef) {

        this.dataSet = [];
        this.suggestData = this.dataSet;

    }

    @Input()
    set label(value: string) {
        this.labelTxt = value;
    }

    @Input()
    set dataSource(value: string[]) {
        this.dataSet = value;
    }

    ngAfterViewChecked(): void {
        var ele = this.ref.nativeElement.getElementsByTagName("ul");
    }

    ngAfterViewInit(): void {
        this.inputElement = this.ref.nativeElement.getElementsByTagName("input")[0];
        this.ionItem = this.ref.nativeElement.getElementsByTagName("ion-item")[0];
    }

    @Input()
    get value(): string {

        return this.val;
    }

    set value(val: string) {
        this.val = val + "";
    }

    private suggest(value): void {
        this.suggestData = this.dataSet.filter((data) => {
            return data.toLowerCase().indexOf(value.toLowerCase()) > -1;
        });

        if (this.suggestData.length) {
            this.showSuggest = true;

        }
        else {
            this.showSuggest = false;
        }
    }

    modalChangedEvent($event): void {
        this.propagateChange($event);
        this.inputElement.value = $event;
        this.val = $event;

        if (!$event.length) {
            this.showSuggest = false;
        }

        if ($event.length > 2) {
            this.suggest($event);
        }
        this.replaceClass();
    }

    writeValue(value: string): void {
        this.val = value;
        this.replaceClass();
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fnc) {

    }

    private replaceClass(): void {

        if (!this.ionItem)
            return;

        let className = this.ionItem.className;

        className = className.replace(" custom-invalid", "");

        if (!this.val) {
            className = className + " custom-invalid";
        }


        this.ionItem.className = className;
    }

    inputFocused(value: boolean): void {
       setTimeout(() => {
            this.showSuggest = value;
            this.changeRef.detectChanges();
        }, 100)
    }

    selectedValue(value: string): void {

        this.val = value;
        this.propagateChange(this.val);

    }

}