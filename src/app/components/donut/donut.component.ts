import { Component, Input} from '@angular/core';


@Component({
    templateUrl: 'donut.component.html',
    styles: ['donut.component.css'],
    selector: 'cab-donut'    
})

export class DonutComponent {    
   
   @Input()
    time:number;  

}