import { Component, Input } from '@angular/core';

@Component({
    templateUrl: "driver-details.component.html",
    selector: "driver-details"
})

export class DriverDetailsComponent {

    show: boolean = false;
    private timeOut = 10000;
    driver: any;
    private handle:any;

   

    @Input()
    set details(data: any) {
        this.driver = data;
        this.show= true;
        if(this.handle)
            clearTimeout(this.handle);

        this.handle = setTimeout(() => {
            this.show = false;
        }, this.timeOut);
     
    }

    get details(): any {
        return this.details;
    }

    showDialog():void{
        this.show = !this.show;
    }

}