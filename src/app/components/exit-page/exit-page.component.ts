import { Component, AfterViewInit } from '@angular/core';
import { Platform } from 'ionic-angular';
import { PageNavigationProvider } from '../../providers/page-navigation.provider';
import { BackgroundMode } from '@ionic-native/background-mode';
import { Keyboard } from '@ionic-native/keyboard';


@Component({
    templateUrl: "exit-page.component.html",
    selector: "exit-page"   
})


export class ExitPageComponent implements AfterViewInit {
    constructor(private navigator: PageNavigationProvider, private keyboard: Keyboard, private platform:Platform, private backgroundMode: BackgroundMode) {
        
     }

     ngAfterViewInit():void{
        this.platform.registerBackButtonAction(() =>{
            if(this.navigator.hasNavigation()){
                this.returnToPrevious();
            }
            else{
                this.platform.registerBackButtonAction(() =>{
                    this.backgroundMode.moveToBackground();
                    this.backgroundMode.disableWebViewOptimizations();
                });

                this.backgroundMode.moveToBackground();
                this.backgroundMode.disableWebViewOptimizations();
            }
        })
     }

    returnToPrevious(): void {
        this.keyboard.close();
        this.navigator.return();    
    }

}