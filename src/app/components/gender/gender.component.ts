import { Component, Input, forwardRef } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: "gender",
    templateUrl: "gender.component.html",
    providers: [{ provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(() => GenderComponent), multi: true }]
})

export class GenderComponent implements ControlValueAccessor {

    private genderInput: string;
    private propagateChange = (_:any) =>{};

    @Input()
    set gender(genderInput: string) {
        this.gender = genderInput.toLowerCase();
    }

    get gender(): string {
        return this.genderInput;
    }

    getActiveClass(genderType: any): string {
        if (this.genderInput == "male" && genderType == "m")
            return "touched active";

        if (this.genderInput == "female" && genderType == "f")
            return "touched active";

        return "touched";
    }

    writeValue(value: string) {
        if (value) {
            this.genderInput = value.toLocaleLowerCase();
        }

    }

    onGenderSelect(genderType: string): void {
        this.genderInput = (genderType == "m") ? "male" : "female";
        this.propagateChange(this.genderInput);
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fn) {

    }
}