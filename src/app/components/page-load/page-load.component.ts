import { Component, Input } from '@angular/core';

@Component({
    templateUrl: "page-load.component.html",
    selector: "page-load"
})

export class PageLoadComponent {

    dialogMessage: string;
    showDialog:boolean;

    @Input()
    set message(msg: string) {
        this.dialogMessage = msg;       
    }

    @Input()
    set show(showDlg:boolean){
        this.showDialog = showDlg;      
    }
}