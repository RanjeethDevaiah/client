import { Component, Input } from '@angular/core'

@Component({
    selector:"page-title",
    templateUrl:"page-title.component.html"
})


export class PageTitleComponent{

    titleText:string;
    @Input() 
    set title(title:string){
        this.titleText = title;
    }
}