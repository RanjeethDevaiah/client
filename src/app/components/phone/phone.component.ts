import { Component, Input, forwardRef, ElementRef, AfterViewInit } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    templateUrl: "phone.component.html",
    selector: "phone-control",
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => PhoneComponent),
        multi: true
    }]
})

export class PhoneComponent implements ControlValueAccessor, AfterViewInit {
    phoneNumber: string = "";
    isReadOnly:boolean = false;
    private propagateChange = (_: any) => { };
    private inputElement: HTMLInputElement;
    private ionItem: HTMLElement;

    constructor(private ref: ElementRef) {

    }

    @Input()
    set readonly(value:boolean){
        this.isReadOnly = value;
    }

    get readonly():boolean{
        return this.isReadOnly;
    }

    ngAfterViewInit(): void {
        this.inputElement = this.ref.nativeElement.getElementsByTagName("input")[0];
        this.ionItem = this.ref.nativeElement.getElementsByTagName("ion-item")[0];
    }

    private format(value: string): string {

        if (!value) {
            return "";
        }

        var digits = value.split('');



        switch (digits.length) {
            case 1:
                return `${digits[0]}`;
            case 2:
                return `${digits[0]}${digits[1]}`;
            case 3:
                return `( ${digits[0]}${digits[1]}${digits[2]} )`;
            case 4:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}`;
            case 5:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}`;
            case 6:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}${digits[5]} `;
            case 7:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}${digits[5]} - ${digits[6]}`;
            case 8:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}${digits[5]} - ${digits[6]}${digits[7]}`;
            case 9:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}${digits[5]} - ${digits[6]}${digits[7]}${digits[8]}`;
            default:
                return `( ${digits[0]}${digits[1]}${digits[2]} ) ${digits[3]}${digits[4]}${digits[5]} - ${digits[6]}${digits[7]}${digits[8]}${digits[9]}`;
        }
    }


    private convertTextToNum(value: string): string {
        if (!value)
            return "";

        var digits = value.replace(/[^0-9.]/g, "");

        if (!digits)
            return "";

        if (digits.length > 10)
            return digits.substring(0, 10);
        else
            return digits;
    }

    @Input()
    get numbers(): number {

        return +this.convertTextToNum(this.phoneNumber);
    }

    set numbers(val: number) {
        this.phoneNumber = val + "";
    }

    modalChangedEvent($event): void {
        let number = this.convertTextToNum($event);

        if (number.length > 9)
            this.propagateChange(+number);

        this.phoneNumber = this.format(number);

        this.inputElement.value = this.phoneNumber;
        this.replaceClass();
    }

    writeValue(value: number): void {
        if (value) {
            this.phoneNumber = this.format(value + "");
        }
        else {
            this.phoneNumber = "";
        }

        this.replaceClass();
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fun) {

    }

    private replaceClass(): void {

        if (!this.ionItem)
            return;

        let className = this.ionItem.className;

        className = className.replace(" custom-invalid", "");

        if (this.phoneNumber.length > 0 && this.phoneNumber.length < 18) {
            className = className + " custom-invalid";
        }


        this.ionItem.className = className;
    }

}