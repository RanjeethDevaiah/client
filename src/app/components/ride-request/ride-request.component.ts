import { Component, Input, Output, EventEmitter } from '@angular/core';

import { Dialogs } from "@ionic-native/dialogs"

@Component({
    templateUrl: 'ride-request.component.html',
    selector: 'ride-request'
})

export class RideRequestComponent {
    private data: any;
    private handle: any;
    private handleDonutTimer: any;
    private beepHandle: any;
    showWait: boolean;
    time: number;

    constructor(private dialogs: Dialogs) {
        
     }

    private beep(interval: number): void {
        this.dialogs.beep(1);

        this.beepHandle = setTimeout(() => {
            this.beep(2000);
        }, interval);
    }

    private beepTurnOff(): void {
        if (this.beepHandle) {
            clearTimeout(this.beepHandle);
        }
    }


    @Input()
    set request(request: any) {
        this.data = request;
        if (this.data) {
            this.showWait = false;
            this.beep(2000);

            if (this.handle)
                clearTimeout(this.handle);

            this.handle = setTimeout(() => {
                this.onRejected();
            }, 12000);
        }
    }

    @Input()
    set waitTime(delayTime: number) {
        if (delayTime) {
            if (this.handleDonutTimer)
                clearInterval(this.handleDonutTimer);
            this.showWait = true;
            this.time = delayTime;
            this.handleDonutTimer = setInterval(() => {
                this.time = this.time - 1;
                if (!this.time) {
                    clearInterval(this.handleDonutTimer);
                    this.waitComplete.emit();
                }
                else {
                    if (this.time % 10 === 0)
                        this.interrupt.emit();
                }
            }, 1000)
        }
        else {
            if (this.handleDonutTimer)
                clearInterval(this.handleDonutTimer);
        }
    }

    get request(): any {
        return this.data;
    }

    @Output()
    accepted: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    rejected: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    waitComplete: EventEmitter<void> = new EventEmitter<void>();

    @Output()
    interrupt: EventEmitter<void> = new EventEmitter<void>();


    onAccepted(): void {
        clearTimeout(this.handle);
        this.beepTurnOff();
        this.accepted.emit();
    }

    onRejected(): void {
        this.beepTurnOff();
        this.rejected.emit();
    }

}