import { Component, AfterViewInit, ViewChild, ElementRef } from '@angular/core';

import { MenuController } from 'ionic-angular';

import { MapService } from '../../providers/map.provider';
import { ActionTransfer } from '../../providers/action-transfer.provider';


@Component({
    templateUrl: "root.component.html",
    styles: [`root.component.css`]
})

export class RootComponent implements AfterViewInit {

    @ViewChild('map') mapRef: ElementRef;
    private mapElement: HTMLDivElement;


    constructor(private menu: MenuController, private map: MapService, private action: ActionTransfer) {
        menu.enable(true, 'mainMenu');
    }


    ngAfterViewInit(): void {
        this.action.onComponentLoaded()
            .subscribe(() => {
                this.mapElement = this.mapRef.nativeElement;
                this.map.loadMap(this.mapElement)
                    .then(() => {
                        this.action.intializedMap();
                    });
            });
    }
}