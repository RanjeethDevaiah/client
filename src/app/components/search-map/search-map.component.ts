import { Component, AfterViewInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { MapService } from '../../providers/map.provider';

@Component({
    selector: 'search-map',
    templateUrl: 'search-map.component.html',
    styles: ['search-map.component.css']
})

export class SearchMapComponent implements AfterViewInit {

    @ViewChild('from') fromRef: ElementRef;
    @ViewChild('to') toRef: ElementRef;

    @Output() elements:EventEmitter<any> = new EventEmitter();

    private fav:boolean = false;
    private home:boolean = false;

    constructor(private map: MapService) { }

    ngAfterViewInit(): void {
        this.elements.emit({ from: this.fromRef.nativeElement, to: this.toRef.nativeElement });
    }

    loadInitial():void{

        var places = this.map.getDestinations();

        if (places.length > 0)
            if (this.fromRef.nativeElement.value !== this.map.getDestinations()[0].address)
                this.fromRef.nativeElement.value = this.map.getDestinations()[0].address;

        if (places.length > 1)
            if (this.toRef.nativeElement.value !== this.map.getDestinations()[1].address)
                this.toRef.nativeElement.value = this.map.getDestinations()[1].address;

    }    

   
    pressTo(name:string):void{
        if (name === 'from')
            this.fromRef.nativeElement.value = '';
        if (name === 'to')
            this.toRef.nativeElement.value = '';
    }

    isHomeRoute():void {
        this.home = !this.home;
        this.fav = false;
    }

     isFavRoute():void {
        this.home = false;
        this.fav = !this.fav;
    }

    getClass(index:number): string {

        if(index === 0 && this.home || index === 1 && this.fav)
             return 'active'

        else '';
    }
}