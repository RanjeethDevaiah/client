import { Component, Input } from "@angular/core";

@Component({
    templateUrl:"seek-ride.component.html",
    selector:"seek-ride"
})

export class SeekRideComponent {

   showDialog:boolean =false;
    @Input()
    set show(show:boolean){
        this.showDialog = show;
    }

}