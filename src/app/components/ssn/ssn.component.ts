import { Component, Input, forwardRef, ElementRef, AfterViewInit } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    templateUrl: "ssn.component.html",
    selector: "ssn-control",
    providers: [{
        provide: NG_VALUE_ACCESSOR,
        useExisting: forwardRef(() => SsnComponent),
        multi: true
    }]
})

export class SsnComponent implements ControlValueAccessor, AfterViewInit {
    ssnNumber: string = "";
    private propagateChange = (_: any) => { };
    private inputElement: HTMLInputElement;
    private ionItem: HTMLElement;
    isReadOnly: boolean = false;
    masked: string = "";

    constructor(private ref: ElementRef) {

    }

    @Input()
    set readonly(value: boolean) {
        this.isReadOnly = value;
    }

    get readonly(): boolean {
        return this.isReadOnly;
    }

    ngAfterViewInit(): void {
        this.inputElement = this.ref.nativeElement.getElementsByTagName("input")[0];
        this.ionItem = this.ref.nativeElement.getElementsByTagName("ion-item")[0];
    }

    private format(value: string): string {

        if (!value) {
            return "";
        }

        var digits = value.split('');


        switch (digits.length) {
            case 1:
                return `${digits[0]}`;
            case 2:
                return `${digits[0]}${digits[1]}`;
            case 3:
                return `${digits[0]}${digits[1]}${digits[2]}`;
            case 4:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}`;
            case 5:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}${digits[4]}`;
            case 6:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}${digits[4]} - ${digits[5]}`;
            case 7:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}${digits[4]} - ${digits[5]}${digits[6]}`;
            case 8:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}${digits[4]} - ${digits[5]}${digits[6]}${digits[7]}`;
            default:
                return `${digits[0]}${digits[1]}${digits[2]} - ${digits[3]}${digits[4]} - ${digits[5]}${digits[6]}${digits[7]}${digits[8]}`;
        }
    }

    private mask(value: string): string {

        if (!value) {
            return "";
        }

        var digits = value.split('');
        return `* * * - * * - ${digits[5]}${digits[6]}${digits[7]}${digits[8]}`;
    }


    private convertTextToNum(value: string): string {
        if (!value)
            return "";

        var digits = value.replace(/[^0-9.]/g, "");

        if (!digits)
            return "";

        if (digits.length > 9)
            return digits.substring(0, 9);
        else
            return digits;
    }

    @Input()
    get numbers(): number {

        return +this.convertTextToNum(this.ssnNumber);
    }

    set numbers(val: number) {
        this.ssnNumber = val + "";
    }

    modalChangedEvent($event): void {
        if ($event && $event.indexOf('* * * - * * -') >-1 && $event.length>17) {
            this.inputElement.value = $event.substring(0, 18);
            return;
        }

        let number = this.convertTextToNum($event);
        this.ssnNumber = this.format(number);

        if (number.length > 8) {
            this.propagateChange(+number);
            this.inputElement.value = this.mask(number);
        }
        else {
            this.inputElement.value = this.ssnNumber;
        }

        this.inputElement.setSelectionRange(this.inputElement.value.length, this.inputElement.value.length);
        this.replaceClass();
    }

    writeValue(value: number): void {
        if (value) {
            this.ssnNumber = this.mask(value + "");
            if (this.inputElement)
                this.inputElement.value = this.ssnNumber;
        }
        else {
            this.ssnNumber = "";
        }
        this.replaceClass();
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
    }

    registerOnTouched(fun) {

    }

    private replaceClass(): void {

        if (!this.ionItem)
            return;

        let className = this.ionItem.className;

        className = className.replace(" custom-invalid", "");

        if (this.ssnNumber.length > 0 && this.ssnNumber.length < 13) {
            className = className + " custom-invalid";
        }


        this.ionItem.className = className;
    }

}