import { Component, Output, EventEmitter } from '@angular/core';


@Component({
    selector: 'cab-timer',
    templateUrl: 'timer.component.html',
})

export class TimerComponent {

    private seconds: number = 0;
    private startTimer: boolean = false;
    private listener: any;

    @Output() timerUpdate = new EventEmitter();

    start(): void {
        if (this.startTimer)
            return;

        this.startTimer = true;
        this.listener = setInterval(() => {
            this.seconds = this.seconds + 1;
            this.timerUpdate.emit(this.seconds);
        }, 1000);
    }
    stop(): void {
        if (this.listener)
            clearInterval(this.listener);

        this.startTimer = false;
    }

    reset(): void {
        this.stop();
        this.seconds = 0;
        this.timerUpdate.emit(this.seconds);
    }

    toggle(): void {
        if (this.startTimer)
            this.stop();
        else
            this.start();
    }

    getHour(): string {

        return '' + Math.floor(this.seconds / 3600);
    }

    getMins(): string {
        return '' + Math.floor(this.seconds / 60);
    }

    getSeconds(): string {
        return (this.seconds % 60 > 9) ? '' + this.seconds % 60 : '0' + this.seconds % 60;
    }
}