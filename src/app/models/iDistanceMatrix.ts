export interface IDistanceMatrix {
    value: {
        distance: number;
        duration: number;
    },

    text: {
        distance: string;
        duration: string;
    }

}