import { ILocation } from './iLocation';
import { IReverseGeoLocation } from './iReverseGeo';


export interface IPlace {
    location:ILocation;
    address:string;
    geo ?:IReverseGeoLocation;
}