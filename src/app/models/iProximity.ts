export interface IProximity{
    customer:string,    
    distance:number,
    duration:number,
    text:{
        duration:string,
        distance:string,
    }
}