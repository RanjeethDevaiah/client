export interface IRegistration {
    profile: string;
    clientId: string;    
    addtionalData?:any;
}

export interface IProfile {
    first_name: string;
    last_name: string;
    phone: Number;
    email: string;
    override?: boolean;
    is_guest?: boolean;
    is_driver?: boolean;
    image?:any;
}