import { ILocation } from './iLocation';

export interface IReverseGeoLocation{
    address:string;
    city:string;
    location:ILocation;
}