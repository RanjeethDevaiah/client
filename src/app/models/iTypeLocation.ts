import { ILocation } from './iLocation';

export interface ITypeLocation {
    id:string,
    location:ILocation,
    type:number
}