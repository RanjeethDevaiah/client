export class Payment {
    
    maskedNumber: string;
    expirationDate: string;
    cardType: string;

    constructor(private data: any) {
        this.maskedNumber = data.maskedNumber;
        this.expirationDate = data.expirationDate;
        this.cardType = data.cardType.toLowerCase();
    }
}