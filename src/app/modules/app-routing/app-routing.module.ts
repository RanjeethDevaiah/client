import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { MainPageComponent } from '../../pages/main/main-page.component';
import { BookingPageComponent } from '../../pages/main/booking/booking-page.component';
import { DrivePageComponent } from '../../pages/main/drive/drive-page.component';
import { RegistrationPageComponent } from '../../pages/shared/registration/registration-page.component';
import { ClientPaymentPageComponent } from '../../pages/shared/payment-client/client-payment-page.component';
import { FavPageComponent } from '../../pages/shared/fav/fav-page.component'
import { LoginPageComponent } from '../../pages/login/login-page.component';
import { TripCompletePageComponent } from '../../pages/shared/trip-complete/trip-complete-page.component';
import { AccountPaymentPageComponent } from '../../pages/shared/payment-account/account-payment-page.component';
import { TermsPageComponent } from '../../pages/shared/terms/terms-page.component';
import { TransactionPageComponent } from '../../pages/shared/transactions/transactions-page.component';
import { AuthCodePageComponent } from '../../pages/shared/auth-code/auth-code-page.component';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },

    {
        path: 'main',
        component: MainPageComponent,
        children: [
            { path: '', redirectTo: 'drive', pathMatch: 'full' },
            {
                path: 'book',
                component: BookingPageComponent,
                children: [{ path: 'register/:mode', component: RegistrationPageComponent },
                { path: 'client-payment', component: ClientPaymentPageComponent },               
                { path: 'complete', component: TripCompletePageComponent },
                { path: 'fav', component: FavPageComponent },
                { path: 'terms', component: TermsPageComponent},
                { path: 'trx', component: TransactionPageComponent}
            ]},
            {
                path: 'drive',
                component: DrivePageComponent,
                children: [{ path: 'register/:mode', component: RegistrationPageComponent },                
                { path: 'account-payment', component: AccountPaymentPageComponent },
                { path: 'complete', component: TripCompletePageComponent },
                { path: 'fav', component: FavPageComponent },
                { path: 'terms', component: TermsPageComponent},
                { path: 'trx', component: TransactionPageComponent}]
            }]
    },
    {
        path: 'login',
        component: LoginPageComponent,
        children: [
            { path: '', component: LoginPageComponent }]
    },
    {
        path: 'shared',
        component: LoginPageComponent,
        children: [{ path: '', redirectTo: '/main', pathMatch: 'full' },
        { path: 'register/:mode/:flow', component: RegistrationPageComponent },
        { path: 'client-payment', component: ClientPaymentPageComponent },
        { path: 'account-payment', component: AccountPaymentPageComponent },
        { path: 'terms', component: TermsPageComponent},
        { path: 'auth/:mode', component:AuthCodePageComponent}]
    }
]


@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})

export class AppRoutingComponent { }
