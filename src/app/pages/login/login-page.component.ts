import { Component } from '@angular/core';
import { StateProvider } from '../../providers/app-state.provider';
import { ActionTransfer } from '../../providers/action-transfer.provider';
import { PageNavigationProvider } from '../../providers/page-navigation.provider';
import { Platform } from 'ionic-angular';
import { BackgroundMode } from '@ionic-native/background-mode';

declare var navigator: any;

@Component({
    templateUrl: 'login-page.component.html'
})

export class LoginPageComponent {

    selected: string = "guest";
    private show: boolean = true;
    init: boolean = false;

    constructor(private state: StateProvider, private action: ActionTransfer, private nav: PageNavigationProvider,
        private platform: Platform, private backgroundMode: BackgroundMode) {
        this.action.onComponentLoaded()
            .subscribe(() => this.initialize());


        platform.registerBackButtonAction(() => {
            backgroundMode.excludeFromTaskList();
            platform.exitApp();
        });

    }

    initialize(): void {
        console.log("Environment initialization");
        this.init = true;
    }

    showTab(): boolean {
        return !this.show;
    }

    private createUrl(resolveTo: string, addFlow?: boolean): any {
        return { url: resolveTo, flow: addFlow };
    }

    private navigateToLocation(scheme: any): void {

        if (scheme.flow)
            this.nav.navigate(scheme.url + "/1");
        else
            this.nav.navigate(scheme.url);
    }

    loginGuest(): void {
        this.state.setGuestMode();
        //check if registered and go to booking or registration screen
        this.state.hasGuestProfile()
            .then((exists) => {
                if (!exists)
                    return this.createUrl("shared/register/guest", true);

                return this.state.hasPaymentDetails();
            })
            .then((result: any) => {
                if (result.url)
                    return result;

                if (!result) {
                    return this.createUrl("shared/client-payment", true);
                }
                else {
                    return this.state.getGuestProfile();
                }
            })
            .then((result) => {
                if (result.url)
                    return result;

                if (result.validated) {
                    return this.createUrl("main/book");
                }

                return this.createUrl("shared/auth/guest")

            })
            .then((urlScheme) => {
                this.navigateToLocation(urlScheme);
            })
            .catch(error => console.log(error));
    }

    loginDriver(): void {
        this.state.setDriverMode();

        this.state.hasDriverProfile()
            .then((exists) => {
                if (!exists)
                    return this.createUrl("shared/register/driver", true);

                return this.state.hasAccountDetails();
            })
            .then((result: any) => {
                if (result.url)
                    return result;


                if (!result) {
                    return this.createUrl("shared/account-payment", true);
                }
                else {
                    return this.state.getDriverProfile();
                }

            })
            .then((result) => {
                if (result.url)
                    return result;

                if (result.validated) {
                    return this.createUrl("main/drive");
                }

                return this.createUrl("shared/auth/driver")
            })
            .then((urlScheme) => {
                this.navigateToLocation(urlScheme);
            })
            .catch(error => console.log(error));
    }
}
