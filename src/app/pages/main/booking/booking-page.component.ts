import { Component, AfterViewInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number';
import { FabContainer, ToastController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { LocationTracker } from '../../../providers/location-tracker.provider';
import { XChangeProvider } from '../../../providers/xchange.provider'
import { ActionTransfer } from '../../../providers/action-transfer.provider';
import { MapService } from '../../../providers/map.provider';
import { NavigationService } from '../../../providers/navigation.provider';
import { StateProvider } from '../../../providers/app-state.provider';
import { AlertController } from 'ionic-angular';
import { SearchMapComponent } from '../../../components/search-map/search-map.component';
import { DriverDetailsComponent } from '../../../components/driver-details/driver-details.component';
import { EventProvider } from '../../../providers/event.provider';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';
import { Subject } from 'rxjs/subject';
import { RouteTracker } from '../../../providers/route-tracker';
import { BookingState } from './booking-state';
import { RidesNearMe } from '../../../providers/rides-near-tracker';

@Component({
    templateUrl: "booking-page.component.html",
})

export class BookingPageComponent implements AfterViewInit {

    @ViewChild(SearchMapComponent) searchComponent;
    @ViewChild(DriverDetailsComponent) driverComponent;

    @ViewChild('fab') fabCon: FabContainer;
    @ViewChild('travelTime') durationEle: ElementRef;


    delay: number;
    hideSearch: boolean = true;
    canRequestRide: boolean = false;
    showTravelTime: boolean = false;
    driver: any;
    uiState: BookingState;
    waitForResp: boolean = false;

    private from: HTMLInputElement;
    private to: HTMLInputElement;
    private to2: HTMLInputElement;
    private bookingHandle: any;
    private bookingData: any = {};
    private routeTracker: RouteTracker;
    private ridesNearMe: RidesNearMe;

    constructor(
        private ref: ChangeDetectorRef,
        private action: ActionTransfer,
        private alertCtrl: AlertController,
        private map: MapService,
        private nav: NavigationService,
        private locationTracker: LocationTracker,
        private state: StateProvider,
        private xchange: XChangeProvider,
        private eventPrvdr: EventProvider,
        private toastCtrl: ToastController,
        private pageNav: PageNavigationProvider,
        private keyboard: Keyboard,
        private callNumber: CallNumber
    ) {

        this.uiState = new BookingState(this.ref);

        this.state.setGuestMode();
        this.action.onMapInitialized().subscribe(() => {

            this.xchange.setToken()
                .then(() => {
                    this.intialized();
                })
                .catch(error => {
                    console.error(error);
                });
        });
    }

    private unsubscribeToPrevious(subject: Subject<any>): void {
        subject.observers.forEach((observer: any) => {
            observer.unsubscribe();
        });
    }


    intialized(): void {
        this.keyboard.close();

        this.unsubscribeToPrevious(this.map.listenToMapClick());
        this.unsubscribeToPrevious(this.map.onPlaceChangeEvent());
        this.routeTracker = new RouteTracker(this.locationTracker, this.eventPrvdr, this.map, 0);

        this.map.listenToMapClick().subscribe(() => {
            if (!this.hideSearch) {
                this.hideSearch = true;
                this.ref.detectChanges();

            }

            this.keyboard.close();
            this.fabCon.close();
        });

        this.initialState();

        if (!this.eventPrvdr.isInitialized()) {
            this.eventPrvdr.connect()
                .then(state => {
                    this.createEventSubscription();
                    console.log("subscribed to Event");
                })
                .catch(error => console.log(error));
        }
        else {
            this.createEventSubscription();
            console.log("resubscribed to Event");
        }

        this.state.hasLatestTripComplete()
            .then((result: boolean) => {
                if (result) {
                    this.pageNav.navigate("main/book/complete");
                }
            });


        this.map.onPlaceChangeEvent()
            .subscribe(() => {
                this.keyboard.close();
                this.fillTxt2Box();
                this.calculateTotalDistance();
            });

        this.map.onCurrentLocationEvent()
            .subscribe(location => {
                if (this.ridesNearMe) {
                    this.ridesNearMe.stop();
                }

                this.ridesNearMe = new RidesNearMe(location, this.map, this.xchange);
                this.ridesNearMe.start();
            });


        this.action.onFavSelected()
            .subscribe((fav: any) => {
                console.log(fav);
                this.map.setEndPlace({
                    address: fav.end_address,
                    location: fav.end_loc,
                    geo: fav.end_geo
                });
                this.calculateTotalDistance();
                this.ref.detectChanges();

            });
    }

    private createEventSubscription(): void {
        this.subscribeToRideCancellation();
        this.subscribeToReachedLoaction();
        this.subscribeToTripStarted();
        this.subscribeToTripComplete();
        this.subscribeToRideTracking();
    }

    private initialState(): void {
        if (this.to2)
            this.to2.value = '';

        this.locationTracker.getCurrentLocation()
            .then(location => {
                this.map.resetToInitial(location);
                return this.map.reverseGeoLocation(location);
            })
            .catch(error => {
                console.error(error);
            });
    }

    ngAfterViewInit(): void {
        this.action.onMapInitialized().subscribe(() => {
            this.map.setFromSearchInput(this.from);
            this.map.setToSearchInput(this.to);
            this.to2 = <HTMLInputElement>document.getElementById("secondInput");

            this.map.setToSearchInput2(this.to2);
        });
    }

    pressTo2(event: any): void {
        this.to2.value = '';
    }

    private subscribeToRideTracking(): void {
        this.eventPrvdr.subscribeToTrackRideLocation()
            .subscribe((event: any) => {
                this.map.startNavigation(event.data, this.bookingData.type);
            });
    }

    private subscribeToTripComplete(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToTripComplete());

        this.eventPrvdr.subscribeToTripComplete()
            .subscribe(event => {
                this.state.setTripComplete(event.data.tripId, event.data.driver, event.data.cost.amount)
                    .then(() => {
                        let places = this.map.getDestinations();
                        return this.state.setTripLocation(this.bookingData.tripId, places[0], places[1]);
                    })
                    .then(() => {
                        this.uiState.setCurrentState(BookingState.STATE_INITIAL);
                        this.initialState();
                        return this.state.deleteGuestState();
                    })
                    .then(() => {
                        this.pageNav.navigate("main/book/complete");
                        this.ref.detectChanges();
                    });
            });
    }

    private subscribeToRideCancellation(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToDriverRideCancellationEvent());
        this.routeTracker.stop();
        this.eventPrvdr.subscribeToDriverRideCancellationEvent()
            .subscribe(event => {
                this.map.removeDelayPoint();

                let toast = this.toastCtrl.create({
                    message: event.data.details,
                    position: "middle",
                    showCloseButton: true,
                    closeButtonText: "OK"

                });

                toast.onDidDismiss(() => {
                    console.log("Driver cancellation received.");
                    this.uiState.setCurrentState(BookingState.STATE_INITIAL);
                    this.ref.detectChanges();

                });

                toast.present();
            });
    }

    private fillTxt2Box(): void {
        var places = this.map.getDestinations();

        if (places.length > 1)
            if (this.to2.value !== this.map.getDestinations()[1].address)
                this.to2.value = this.map.getDestinations()[1].address;

    }

    toggleSearch(fab: FabContainer): void {
        this.searchComponent.loadInitial();
        this.hideSearch = !this.hideSearch;
        fab.close();
    }

    showFav(fab: FabContainer): void {
        this.pageNav.navigate('main/book/fav', [{ show_fav: true }]);
        fab.close();
    }

    getRideHome(fab: FabContainer): void {

        this.state.getLocationTag()
        .then((data:any) => {           
            this.action.invokeFavSelected(data[1]);
        })
        .catch(error => {
            console.error(error);
        });

        fab.close();
    }

    callDriver(fab: FabContainer): void {

        fab.close();
    }

    showDetails(fab: FabContainer): void {
        this.driverComponent.showDialog();
        fab.close();
    }

    showOptions(): void {
        this.canRequestRide = false;
    }

    placeBooking(value: number, type: number): void {
        this.uiState.setCurrentState(BookingState.STATE_SEEK);
        this.ridesNearMe.stop();
        let destionations = this.map.getDestinations();
        this.map.setToCenter();
        this.map.clearRidesToLocation();

        this.xchange.seekRide(destionations[0],
            (destionations.length > 1) ? destionations[1] : null, value, type)
            .then(tripId => {
                console.log("Booking request sent");
                this.map.clearRidesToLocation();

                if (this.bookingHandle)
                    clearTimeout(this.bookingHandle);

                this.bookingHandle = setTimeout(() => {
                    this.getPooledDrivers(tripId, type);
                }, 5000);
            })
            .catch(error => {
                console.error("Booking request error " + error);
                this.uiState.setCurrentState(BookingState.STATE_INITIAL);
                this.ridesNearMe.start();
            });
    }


    calculateTotalDistance(): void {
        this.waitForResp = true;
        let destinations = this.map.getDestinations();
        this.xchange.getApproximate(destinations[0].location, destinations[1].location, destinations[0].geo.city)
            .then((result: any) => {

                let value = result.distance.value;
                let val = result.distance.text;
                let taxi = {
                    duration: (result["1"]) ? result["1"].text.duration : '<i class="material-icons">not_interested</i>',
                    amount: (result["1"]) ? result["1"].amount : '<i class="material-icons">not_interested</i>',
                }

                let share = {
                    duration: (result["2"]) ? result["2"].text.duration : '<i class="material-icons">not_interested</i>',
                    amount: (result["2"]) ? result["2"].text.amount : '<i class="material-icons">not_interested</i>',
                }

                let temp = `                         
                <table class="alert-tbl">
                    <tr>
                       <th><i class="material-icons">directions_car</i></th>
                       <th><i class="material-icons">attach_money</i></th>
                       <th><i class="material-icons">timer</i></th>
                    </tr>
                    <tr>
                        <td>Taxi</td>
                        <td>${taxi.amount} USD</td>
                        <td>${taxi.duration} approx</td>
                    </tr>
                    <tr>
                        <td>Share</td>
                        <td>${share.amount} USD</td>
                        <td>${share.duration} approx</td>
                    </tr>
                </table>
                `

                //this.hideSearch = true;
                let alert = this.alertCtrl.create({
                    title: 'Confirmation',
                    message: ` Approximate distance is ${val}. Do you want to goahead and request a ride ? <br/>${temp}`,
                    buttons: [
                        {
                            text: 'Nearest',
                            handler: () => {
                                if (this.ridesNearMe) {
                                    this.ridesNearMe.stop();
                                }
                                this.placeBooking(value, 0);
                            }
                        },
                        {
                            text: 'Share',
                            handler: () => {
                                if (this.ridesNearMe) {
                                    this.ridesNearMe.stop();
                                }
                                this.placeBooking(value, 1);
                            }
                        },
                        {
                            text: 'Taxi',
                            handler: () => {
                                if (this.ridesNearMe) {
                                    this.ridesNearMe.stop();
                                }
                                this.placeBooking(value, 2);
                            }
                        },

                        {
                            text: 'Ignore',
                            role: 'cancel'
                        }

                    ]
                });
                this.waitForResp = false;
                alert.present();

            }).catch(error => {
                this.noRidesFound();
            });
    }

    private noRidesFound(): void {
        let toast = this.toastCtrl.create({
            message: "Sorry there are no rides available at this location right now.",
            duration: 10000,
            showCloseButton: true,
            closeButtonText: 'Ok',
            position: 'middle'
        });
        toast.present();
        this.waitForResp = false;
    }


    private getPooledDrivers(tripId,type): void {

        console.log("Requesting for drivers info..");
        this.xchange.requestForPooledDrivers(tripId,type)
            .then(details => {
                if (details) {
                    this.bookingData.tripId = details.tripId;
                    this.bookingData.driverId = details.id;
                    this.bookingData.type = details.type;
                    this.xchange.proceedToLocation(details.tripId, details.id)
                        .then(result => {
                            if (result.status) {
                                this.driver = result.details;
                                this.delay = result.duration;
                                this.map.addDelayPoint(this.durationEle.nativeElement);
                                this.uiState.setCurrentState(BookingState.STATE_BOOKED);

                                return this.bookingData;

                            }
                            else {
                                console.log("Threshold to accept the ride reached.");
                                this.state.deleteGuestState();
                            }
                        })
                        .then((data: any) => {
                            if (!data)
                                return;

                            data.start = this.map.getDestinations()[0];
                            data.end = this.map.getDestinations()[1];
                            this.state.setGuestState(data);

                            this.ref.detectChanges();
                        });
                }
                else {

                    if (this.bookingHandle)
                        clearTimeout(this.bookingHandle);

                    this.bookingHandle = setTimeout(() => {
                        this.getPooledDrivers(tripId, type);
                    }, 3000);
                }
            })
            .catch(error => {
                console.log('error while retreiving driver information ' + error);
                this.uiState.setCurrentState(BookingState.STATE_INITIAL);

                this.noRidesFound();
            })
    }

    cancelRide(fab: FabContainer): void {
        fab.close();
        this.xchange.tripCanceledCustomer(this.bookingData.driverId, this.bookingData.tripId)
            .then(result => {
                this.uiState.setCurrentState(BookingState.STATE_INITIAL);
                this.map.removeDelayPoint();
                this.bookingData = {};
                this.routeTracker.stop();
                this.initialState();
                return this.state.deleteGuestState();
            })
            .then(() => {
                console.log("Deleted state");
            })
            .catch(error => {
                console.error(error);
            })
    }

    searchInputs(inputs: Object): void {
        this.from = inputs["from"];
        this.to = inputs["to"];
    }

    private subscribeToReachedLoaction(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToReachedLocatiion());

        this.eventPrvdr.subscribeToReachedLocatiion()
            .subscribe(event => {
                console.dir(event);
                this.map.removeDelayPoint();

                this.routeTracker.start(this.bookingData.tripId);

                let toast = this.toastCtrl.create({
                    message: "The ride has reached your location.",
                    duration: 10000,
                    showCloseButton: true,
                    closeButtonText: 'Ok',
                    position: 'middle'
                });
                toast.present();
            });
    }

    private subscribeToTripStarted(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToTripStarted());

        this.eventPrvdr.subscribeToTripStarted()
            .subscribe(event => {
                this.uiState.setCurrentState(BookingState.STATE_STARTED);
                this.routeTracker.stop();

                let toast = this.toastCtrl.create({
                    message: "Trip started.",
                    position: "middle",
                    duration: 3000
                });

                toast.present();
            });
    }
}