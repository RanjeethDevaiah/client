import { ChangeDetectorRef } from '@angular/core';

export class BookingState {

    public static get STATE_INITIAL(): number {
        return 0;
    }

    public static get STATE_SEEK(): number {
        return 1;
    }

    public static get STATE_BOOKED(): number {
        return 2;
    }

    public static get STATE_COMPLETE(): number {
        return 3;
    }

    public static get STATE_FEEDBACK(): number {
        return 4;
    }

     public static get STATE_STARTED(): number {
        return 5;
    }


    private state: number = BookingState.STATE_INITIAL;

    constructor(private ref:ChangeDetectorRef){

    }


    getCurrentState(): number {
        return this.state;
    }

    setCurrentState(state: number): void {
        this.state = state;
        this.ref.detectChanges();
    }

    initialState(): boolean {
        if (this.state === BookingState.STATE_INITIAL)
            return true;
        else
            return false;
    }


    bookedState(): boolean {
        if (this.state === BookingState.STATE_BOOKED)
            return true;
        else
            return false;
    }
    completedState():boolean{
         if (this.state === BookingState.STATE_COMPLETE)
            return true;
        else
            return false;
    }

    seekState():boolean{
         if (this.state === BookingState.STATE_SEEK)
            return true;
        else
            return false;
    }

    startedState():boolean{
        if (this.state === BookingState.STATE_STARTED)
            return true;
        else
            return false;
    }




}