import { Component, ChangeDetectorRef } from '@angular/core';
import { FabContainer, ToastController, AlertController } from 'ionic-angular';
import { Subject } from 'rxjs/subject';
import { Keyboard } from '@ionic-native/keyboard';
import { DelayCalculation } from "../../../providers/delay-calculation";
import { CallNumber } from '@ionic-native/call-number';
import { ActionTransfer } from '../../../providers/action-transfer.provider';
import { MapService } from '../../../providers/map.provider';
import { NavigationService } from '../../../providers/navigation.provider';
import { LocationTracker } from '../../../providers/location-tracker.provider';
import { XChangeProvider } from '../../../providers/xchange.provider';
import { EventProvider } from '../../../providers/event.provider'
import { StateProvider } from '../../../providers/app-state.provider';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';
import { UUID } from 'angular2-uuid';
import { DriveState } from './drive-state';

import { UploadWayPoints } from '../../../providers/wayPoints-upload';
import { LocationMonitor } from '../../../providers/location-monitor';
import { RouteTracker } from '../../../providers/route-tracker';

@Component({
    templateUrl: 'drive-page.component.html'

})

export class DrivePageComponent {
    state: DriveState;

    showNav: boolean = false;

    showDialog: boolean = false;
    details: any;
    waitTimeOut: number;
    private uploadWayPoints: UploadWayPoints;
    private locMonitor: LocationMonitor;
    private routeTracker: RouteTracker;
    waitForResp: boolean = false;

    constructor(
        private map: MapService,
        private nav: NavigationService,
        private action: ActionTransfer,
        private locationTracker: LocationTracker,
        private toastCtrl: ToastController,
        private xchange: XChangeProvider,
        private statePrvdr: StateProvider,
        private eventPrvdr: EventProvider,
        private ref: ChangeDetectorRef,
        private pageNav: PageNavigationProvider,
        private keyboard: Keyboard,
        private delayCalculation: DelayCalculation,
        private callNumber: CallNumber,
        private alertCtrl: AlertController
    ) {
        this.state = new DriveState(this.ref);
        this.statePrvdr.setDriverMode();
        this.action.onComponentLoaded()
            .subscribe((global: any) => {
                this.statePrvdr.getType()
                    .then((type: number) => {
                        this.locMonitor = new LocationMonitor(this.locationTracker, this.eventPrvdr, this.map, type);
                        this.routeTracker = new RouteTracker(this.locationTracker, this.eventPrvdr, this.map, type);
                        this.uploadWayPoints = new UploadWayPoints(global.db, xchange, locationTracker, map, this.eventPrvdr, type);

                        this.action.onMapInitialized().subscribe(() => {
                            this.xchange.setToken()
                                .then(() => {
                                    this.intialized();
                                })
                                .catch(error => {
                                    console.error(error);
                                })
                        });
                    })
            });
        this.keyboard.close();
    }

    intialized(): void {
        if (!this.eventPrvdr.isInitialized()) {
            this.eventPrvdr.connect()
                .then(state => {
                    this.createEventSubscriptions();
                    console.log("Event subscriptions placed");
                })
                .catch(error => console.log(error));
        }
        else {
            this.createEventSubscriptions();
            console.log("Event resubscriptions placed");
        }

        this.locationTracker.getCurrentLocation()
            .then(location => {
                this.map.initLocation(location, true);
            })
            .catch(error => {
                console.error(error);
            });

        this.delayCalculation.subscribeToDelay()
            .subscribe((delaySec: number) => {
                this.details.totalWaitTime += delaySec;
                console.log(`Added delay ${delaySec} and the total is ${this.details.totalWaitTime}`);
            })
    }

    private createEventSubscriptions(): void {

        this.subscribeToRideRequest();
        this.subscribeToRideCancellationRequest();
        this.subscribeToProceedToLocation();
        this.subscribeToRideTracking();
    }

    private unsubscribeToPrevious(subject: Subject<any>): void {
        subject.observers.forEach((observer: any) => {
            observer.unsubscribe();
        });
    }


    private subscribeToRideTracking(): void {
        this.eventPrvdr.subscribeToTrackRideLocation()
            .subscribe((event: any) => {
                this.map.startNavigation(event.data, 0);
            });
    }


    startTrip(): void {

        this.waitForResp = true;

        this.xchange.tripStarted(this.details.id, this.details.tripId)
            .then(() => {
                this.map.startInitialize();
                this.waitForResp = false;
                this.state.setState(DriveState.STATE_START_TRIP);
                this.uploadWayPoints.start(this.details.tripId);

                this.details.state = DriveState.STATE_START_TRIP;
                this.details.totalWaitTime = 0;
                return this.statePrvdr.setDriverState(this.details);

            })
            .then(() => {
                console.log("Save state");
            })
            .catch((error) => {
                console.error(error);
                this.waitForResp = false;
            });
    }

    startNavigationToDest(fab: FabContainer): void {
        var places = this.map.getDestinations();
        if (places.length > 1)
            this.nav.startNavigation(places[0], places[1]);
        else {
            let toast = this.toastCtrl.create({
                message: "No destination specified to navigate",
                duration: 10000,
                showCloseButton: true,
                closeButtonText: 'Ok',
                position: 'middle'
            });
            toast.present();
        }
        fab.close();

    }

    cancelRide(fab: FabContainer): void {
        fab.close();
        this.showNav = false;
        this.waitForResp = true;
        this.xchange.tripCanceledDriver(this.details.id, this.details.tripId, 1)
            .then((success) => {
                return this.exitWait();
            })
            .catch((error) => {
                console.error(error);
                this.waitForResp = false;
            });
        this.routeTracker.stop();
    }

    startNavigationToPickup(fab: FabContainer): void {
        var places = this.map.getDestinations();
        this.nav.startNavigation(places[1], places[0]);
        fab.close();
    }

    inWait(): void {
        this.map.clearLocationMarker();
        this.showNav = false;
        this.locationTracker.getCurrentLocation()
            .then(location => {
                return this.xchange.registerToQueue(location);
            })
            .then(() => {
                this.waitForResp = false;
                this.state.setState(DriveState.STATE_WAIT);
                this.locMonitor.start();

            })
            .then(() => {
                let toast = this.toastCtrl.create({
                    message: "We are tracking your location. You will be notified of a ride for you near your location.",
                    duration: 3000,
                    showCloseButton: true,
                    closeButtonText: 'Ok',
                    position: 'middle'
                });
                toast.present();
            })
            .catch(error => { console.error(error); this.waitForResp = false; });

        this.waitForResp = true;
    }

    private subscribeToRideRequest(): void {
        var acceptRequests = true;

        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToRideRequestEvent());

        this.eventPrvdr.subscribeToRideRequestEvent()
            .subscribe(event => {
                console.log("Ride request received.");

                if (this.state.getState() === DriveState.STATE_WAIT && acceptRequests) {
                    acceptRequests = false;

                    this.waitForResp = true;
                    this.xchange.ignorePendingRequets(event.data.id, event.data.tripId)
                        .then((result) => {
                            this.details = event.data;
                            this.showDialog = true;
                            this.waitForResp = false;
                            acceptRequests = true;
                            this.state.setState(DriveState.STATE_REQUEST_DISPLAYED);
                        })
                        .catch((error) => {
                            console.error(error);
                            acceptRequests = true;
                            this.waitForResp = false;
                        });
                }
                else {
                    console.log("Ride request ignored since the client is busy processint the  previous requests.");
                }

            });
    }

    onRideAccepted(event: any): void {

        this.waitForResp = true;
        this.xchange.acceptedRideRequest(this.details.id, this.details.tripId)
            .then(result => {
                if (result.status) {
                    this.showNav = true;
                    let data = result.data;
                    this.map.setStartLocation({
                        latitude: data.start_loc.latitude,
                        longitude: data.start_loc.longitude
                    });

                    this.map.setEndLocation({
                        latitude: data.end_loc.latitude,
                        longitude: data.end_loc.longitude
                    });

                    this.waitTimeOut = 60;
                }
                else {
                    this.requestExpired();
                }

                this.waitForResp = false;

            })
            .catch(error => {
                this.requestExpired();
                console.error(error);

                this.waitForResp = false;

            })


    }

    private requestExpired(): void {
        let toast = this.toastCtrl.create({
            message: "Sorry ride request expired",
            duration: 3000,
            showCloseButton: true,
            closeButtonText: 'Ok',
            position: 'middle'
        });
        toast.present();
        this.details = null;
        this.exitWait();
    }

    onRideRejected(event: any): void {
        this.waitForResp = true;
        this.xchange.rejectRide()
            .then((result) => {
                console.log("Trip cancelled");
                this.waitForResp = false;
            })
            .catch((error) => {
                console.error(error);
                this.waitForResp = false;
            })
        this.showDialog = false;
        this.exitWait();
        this.ref.detectChanges();
    }

    private proceedToDest(): void {
        this.state.setState(DriveState.STATE_REACHED);
        this.showDialog = false;
        this.waitTimeOut = 0;
        this.locMonitor.stop();
        this.routeTracker.start(this.details.tripId);

        this.details.state = DriveState.STATE_REACHED;


        this.statePrvdr.setDriverState(this.details)
            .then(() => {
                console.log("Save state");
            });
    }

    private subscribeToProceedToLocation(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToProceedToLocationEvent());

        this.eventPrvdr.subscribeToProceedToLocationEvent()
            .subscribe(event => {
                this.proceedToDest();
            });
    }

    private subscribeToRideCancellationRequest(): void {
        this.unsubscribeToPrevious(this.eventPrvdr.subscribeToCustomerRideCancellationEvent());

        this.eventPrvdr.subscribeToCustomerRideCancellationEvent()
            .subscribe(event => {
                this.routeTracker.stop();
                this.exitWait();
                let toast = this.toastCtrl.create({
                    message: "Customer cancelled the ride. Press wait if you wish to get new ride requests.",
                    position: "middle",
                    showCloseButton: true,
                    closeButtonText: "OK"
                });

                toast.onDidDismiss(() => {
                    console.log("Customer cancellation received.")
                });

                toast.present();
            });
    }

    exitWait(): Promise<any> {
        this.waitForResp = true;
        return this.xchange.exitWaiting()
            .then(() => {
                this.locMonitor.stop();
                this.showNav = false;
                this.state.setState(DriveState.STATE_EXIT_WAIT);
                this.ref.detectChanges();
                return this.statePrvdr.deleteDriverState();
            })
            .then(() => {
                return this.locationTracker.getCurrentLocation();
            })
            .then(current => {
                this.waitForResp = false;
                this.map.resetToInitial(current, true);
            })
            .then(() => {
                let toast = this.toastCtrl.create({
                    message: "You will not receive any ride requests.",
                    duration: 3000,
                    showCloseButton: true,
                    closeButtonText: 'Ok',
                    position: 'middle'
                });
                toast.present();
            })
            .catch(error => {
                console.error(error);
                this.waitForResp = false;
            });
    }

    reachedLocation(): void {
        this.routeTracker.stop();

        this.waitForResp = true;
        this.locationTracker.getCurrentLocation()
            .then(current => {
                return this.xchange.reachedLocation(this.details.id, this.details.tripId, current)
            })
            .then(result => {
                this.waitForResp = false;
                this.state.setState(DriveState.STATE_WAIT_TO_START);
                console.log("Waiting in customer location");

                this.details.state = DriveState.STATE_WAIT_TO_START;

                return this.statePrvdr.setDriverState(this.details);
            })
            .then(() => {
                console.log("Save state");
            })
            .catch(error => {
                console.error(error);
                this.waitForResp = false;
            });
    }

    tripComplete(): void {
        this.map.stopNavigation();
        this.waitForResp = true;
        this.ref.detectChanges();
        this.details.totalWaitTime = Math.round(this.details.totalWaitTime);
        let transactionId = UUID.UUID()
        let payementInfo:any;

        this.uploadWayPoints.stop()
            .then(() => {
                return this.statePrvdr.setTripDetails(this.details.tripId, this.details.id, this.map.getTotalDistance(), this.details.totalWaitTime, transactionId);
            })
            .then(() => {
                console.log("State deleted");
                return this.statePrvdr.deleteDriverState();
            })
            .then(() => {
                return this.xchange.tripComplete(this.details.id, this.details.tripId, this.map.getTotalDistance(), this.details.totalWaitTime);
            })
            .then((result: any) => {
                payementInfo = result; 
                return this.statePrvdr.getType();               
            })
            .then((type:number) =>{
                let prompt = (type === 1)? this.alertCtrl.create({
                    title: "Payment",
                    message: "Total trip amount",
                    inputs: [{
                        name: "amount",
                        value: payementInfo.amount                        
                    }],
                    buttons:[{
                        text:"OK",
                        handler:data =>{
                            console.log(data);
                            this.acceptPayment(transactionId, payementInfo);
                        }
                    }]
                })
                :this.alertCtrl.create({
                    title: "Payment",
                    message: `Total trip amount is ${payementInfo.amount}`,                    
                    buttons:[{
                        text:"OK",
                        handler:data =>{
                            console.log(data);
                            this.acceptPayment(transactionId, payementInfo);
                        }
                    }]
                });

                prompt.present();
            })
    }


    private acceptPayment(transactionId: string, payementInfo: any): Promise<any> {
        let data: any = {
            customer: this.details.id,
            tripId: this.details.tripId,
            transactionId: transactionId
        };

        let paymentData: any;
        paymentData = payementInfo;
        data.amount = paymentData.amount;

        return this.xchange.requestPayment(data)
            .then(() => {
                return this.statePrvdr.setTransaction(this.details.tripId, this.details.id, paymentData.amount, data.transactionId);
            })

            .then(() => {
                return this.statePrvdr.setTripDetailsProcessed(this.details.tripId);
            })
            .then(() => {
                this.showNav = false;
                this.waitForResp = false;
                this.state.setState(DriveState.STATE_DEFAULT);
                return this.locationTracker.getCurrentLocation();
            })
            .then(current => {
                this.map.resetToInitial(current, true);
                this.pageNav.navigate("main/drive/complete");
            })
            .catch(error => {
                this.showNav = false;
                this.waitForResp = false;
                this.state.setState(DriveState.STATE_DEFAULT);
                this.delayCalculation.reset();
                console.log(error);
                this.pageNav.navigate("main/drive/trx");
            });
    }



    onTimeInterrupted(event: any): void {
        this.xchange.checkToProceed(this.details.id, this.details.tripId)
            .then(proceed => {
                if (proceed) {
                    this.proceedToDest();
                }
            })
            .catch(error => {
                console.error(error);
            });
    }

    onWaitComplete(event: any): void {
        this.showDialog = false;
        this.waitTimeOut = 0;
        this.showNav = false;
        this.waitForResp = true;
        this.xchange.tripCanceledDriver(this.details.id, this.details.tripId, 0)
            .then((success) => {
                this.inWait();
                console.log("Wait complete");

                this.waitForResp = false;
            })
            .catch((error) => {
                console.error(error);

                this.waitForResp = false;
            });
    }

}