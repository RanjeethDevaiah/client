import { ChangeDetectorRef } from '@angular/core';

export class DriveState {

    public static get STATE_WAIT(): number { return 0 };
    public static get STATE_EXIT_WAIT(): number { return 1 };
    public static get STATE_START_TRIP(): number { return 2 };
    public static get STATE_STOP_TRIP(): number { return 3 };
    public static get STATE_WAIT_TO_START(): number { return 4 };
    public static get STATE_REACHED(): number { return 5 };
    public static get STATE_REQUEST_DISPLAYED(): number { return 6 };
    public static get STATE_DEFAULT(): number { return 99 };

    constructor(private ref:ChangeDetectorRef){

    }

    private state: number = DriveState.STATE_DEFAULT;

    setState(state: number): void {
        this.state = state;
        this.ref.detectChanges();
    }

    getState(): number {
        return this.state
    }

    inWaiting(): boolean {

        switch (this.state) {
            case DriveState.STATE_WAIT:
                return false;
            case DriveState.STATE_EXIT_WAIT:
                return true;
            case DriveState.STATE_START_TRIP:
                return false;
            case DriveState.STATE_STOP_TRIP:
                return false;
            case DriveState.STATE_WAIT_TO_START:
                return false;
            case DriveState.STATE_REACHED:
                return false;
            case DriveState.STATE_DEFAULT:
                return true;
            default:
                return false;
        }

    }

    endWaiting(): boolean {
        switch (this.state) {
            case DriveState.STATE_WAIT:
                return true;
            case DriveState.STATE_EXIT_WAIT:
                return false;
            case DriveState.STATE_START_TRIP:
                return false;
            case DriveState.STATE_STOP_TRIP:
                return false;
            case DriveState.STATE_WAIT_TO_START:
                return false;
            case DriveState.STATE_REACHED:
                return false;
            case DriveState.STATE_REQUEST_DISPLAYED:
                return true;

            default:
                return false;
        }
    }

    ProceedToCustomerLocation(): boolean {
        switch (this.state) {
            case DriveState.STATE_WAIT:
                return false;
            case DriveState.STATE_EXIT_WAIT:
                return false;
            case DriveState.STATE_START_TRIP:
                return false;
            case DriveState.STATE_STOP_TRIP:
                return false;
            case DriveState.STATE_WAIT_TO_START:
                return false;
            case DriveState.STATE_REACHED:
                return true;
            default:
                return false;
        }
    }

    tripStarted(): boolean {
        switch (this.state) {
            case DriveState.STATE_WAIT:
                return false;
            case DriveState.STATE_EXIT_WAIT:
                return false;
            case DriveState.STATE_START_TRIP:
                return false;
            case DriveState.STATE_STOP_TRIP:
                return true;
            case DriveState.STATE_WAIT_TO_START:
                return true;
            case DriveState.STATE_REACHED:
                return false;
            default:
                return false;
        }
    }

    tripEnd(): boolean {
        switch (this.state) {
            case DriveState.STATE_WAIT:
                return false;
            case DriveState.STATE_EXIT_WAIT:
                return false;
            case DriveState.STATE_START_TRIP:
                return true;
            case DriveState.STATE_STOP_TRIP:
                return false;
            case DriveState.STATE_WAIT_TO_START:
                return false;
            case DriveState.STATE_REACHED:
                return false;
            default:
                return false;
        }

    }

    tripComplete(): boolean {
        switch (this.state) {
            case DriveState.STATE_START_TRIP:
                return true;
            default:
                return false;
        }
    }
}

