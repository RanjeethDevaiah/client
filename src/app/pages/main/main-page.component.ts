import { Component } from '@angular/core';
import { MenuController, Platform } from 'ionic-angular';
import { StateProvider } from '../../providers/app-state.provider';
import { PageNavigationProvider } from '../../providers/page-navigation.provider';
import { RootComponent } from '../../components/root/root.component';
import { BackgroundMode } from '@ionic-native/background-mode';

declare var navigator: any;



@Component({
    templateUrl: "main-page.component.html"
})

export class MainPageComponent {
    rootPage = RootComponent;
    userName:string = "";
    isDriver:boolean = this.appState.isDriverMode();
    isAndroid:boolean = false;
    image:any = "img/no_avatar.jpg";


    constructor(
        private nav: PageNavigationProvider,
        private menu: MenuController,
        private appState: StateProvider,
        private platform: Platform,
        private backgroundMode: BackgroundMode       
    ) {
        nav.reset();
        console.log("main page.");

        if(this.platform.is("android")){
            this.isAndroid = true;
        }       
        

        this.appState.getProfile()
        .then((profile:any) => {
            this.userName = `${profile.last_name}, ${profile.first_name}`;
        })
        .catch(error => console.error(error));
    }

    ngAfterViewInit():void{
        this.appState.getProfile()
        .then((profile:any) => {
            if(profile.image){
                this.image = profile.image;
            }
        });

        this.platform.registerBackButtonAction(()=>{
            this.backgroundMode.disableWebViewOptimizations();
            this.backgroundMode.moveToBackground();
        });
    }

    private getBaseUri(): string {
        if (this.isDriver) {
            return "main/drive/";
        }
        else {
            return  "main/book/";
        }

    }

    openPage(page): void {

        switch (page) {

            case 'payment':
                if (this.appState.isDriverMode()) {
                    this.nav.navigate(this.getBaseUri() + 'account-payment');
                }
                else {
                    this.nav.navigate(this.getBaseUri() + 'client-payment');
                }



                break;

            case 'registraion':

                if (this.appState.isDriverMode()) {
                    this.nav.navigate(this.getBaseUri() + "register", ["driver"]);
                }
                else {
                    this.nav.navigate(this.getBaseUri() + "register", ["guest"]);
                }
                break;

            case 'fav':
                this.nav.navigate(this.getBaseUri() + 'fav');                
                break;

            case 'logout':
                this.backgroundMode.excludeFromTaskList();
                this.platform.exitApp();
                break;

            case 'terms':
                this.nav.navigate(this.getBaseUri() + 'terms');
                break;

            case 'trx':
                this.nav.navigate(this.getBaseUri() + 'trx');
                break;

            default:
                break;

        }
        this.menu.close();
    }
}