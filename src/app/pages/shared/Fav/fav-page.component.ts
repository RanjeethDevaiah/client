import { Component, OnInit } from '@angular/core';
import { StateProvider } from '../../../providers/app-state.provider';
import { ActionTransfer } from '../../../providers/action-transfer.provider';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: "fav-page.component.html"
})

export class FavPageComponent implements OnInit{

    title: string = "Favourite";
    color:any = ["#51B3D7","#EA4055","#78BB65","#EF7B2F","#2A3E51","#D02026","#E45D25","#158BC5","#15A757"];
    tag: string = "home";
    homeTags:[any];
    favTags:[any];

    private show_fav:boolean = false;

    constructor(private appState:StateProvider, 
        private action:ActionTransfer, 
        private nav:PageNavigationProvider,
        private route:ActivatedRoute){

    }

    random(index:number): string {
     // let index =  Math.floor(Math.random() * (8 - 0 + 1)) + 0;

      return this.color[index % 8];
    }

    ngOnInit():void{
        this.show_fav = this.route.snapshot.params['show_fav'];

        if(this.show_fav){
            this.tag = "fav";
        }

        this.appState.getLocationTag()
        .then((data:any) => {
            this.homeTags = data[1];
            this.favTags = data[2];
        })
        .catch(error => {
            console.error(error);
        });
    }

    deleteTag(tag:any):void{
        this.appState.removeLocationTag(tag.rowid)
        .then(() =>{
            console.log("Completed deleting the tag");
        });
    }

    seekCar(tag:any):void{
        this.action.invokeFavSelected(tag);
        this.nav.return();
    }
}