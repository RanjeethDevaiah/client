import { Component, AfterViewInit, NgZone } from '@angular/core';
import { XChangeProvider } from '../../../providers/xchange.provider';
import { StateProvider } from '../../../providers/app-state.provider';
import { ActivatedRoute, Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard';
import { ToastController } from 'ionic-angular';

@Component({
    templateUrl: "auth-code-page.component.html"
})

export class AuthCodePageComponent implements AfterViewInit {

    private mode: string = "";
    title: string = "Authorize";
    waitForResp: boolean = false;
    num1: string = "";
    num2: string = "";
    num3: string = "";
    num4: string = "";

    constructor(private xchange: XChangeProvider,
        private route: ActivatedRoute,
        private zone: NgZone,
        private router: Router,
        private keyboard: Keyboard,
        private state: StateProvider,
        private toastCtrl: ToastController
    ) {

    }

    private randomAuth(): number {
        var val = 0
        while (true) {
            val = Math.floor(Math.random() * 9999) + 1000;
            if (val > 999 && val < 9999)
                return val;
        }
    }

    ngAfterViewInit(): void {
        this.mode = this.route.snapshot.params['mode'];
        this.xchange.sendAuthToken(this.randomAuth(), this.mode === 'driver')
            .then(() => {
                console.log("Completed sending auth code");
            })
            .catch(error => {
                console.error(error);
            })
    }
    modalChangedEvent($event, index) {
       

        switch (index) {
            case 1:
                this.num1 = $event;
                document.getElementById("authId2").focus();
                break;
            case 2:
                this.num2 = $event;
                document.getElementById("authId3").focus();
                break;
            case 3:
                this.num3 = $event;
                document.getElementById("authId4").focus();
                break;
            default:
                this.num4 = $event;
                break;
        }
    }

    onValidate(): void {
        this.waitForResp = true;
        var token = +("" + this.num1 + this.num2 + this.num3 + this.num4);
        this.xchange.validateAuthToken(token, this.mode === 'driver')
            .then(() => {
                if (this.mode === 'driver') {
                    return this.state.getDriverProfile();
                }
                else {
                    return this.state.getGuestProfile();
                }
            })
            .then((profile: any) => {
                profile.validated = true;
                if (this.mode === 'driver') {
                    return this.state.setDriverProfile(profile);
                }
                else {
                    return this.state.setGuestProfile(profile);
                }
            })
            .then(() => {
                setTimeout(() => {
                    this.zone.run(() => {
                        if (this.mode === 'driver') {
                            this.router.navigate(['main/drive']);
                        }
                        else {
                            this.router.navigate(['main/book']);
                        }
                        this.keyboard.close();
                    });
                }, 1000);
            })
            .catch(error => {
                console.log(error);
                this.zone.run(() => {
                    this.waitForResp = false;

                    let toast = this.toastCtrl.create({
                        message: "The Auth code entered is not valid.",
                        position: "middle",
                        showCloseButton: true,
                        closeButtonText: "OK"

                    });

                    toast.onDidDismiss(() => {
                        console.log("user cancellation received.");
                    });

                    toast.present();
                });

            })


    }
}