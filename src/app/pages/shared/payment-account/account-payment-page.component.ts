import { Component, OnInit } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms"
import { ToastController } from "ionic-angular";
import { XChangeProvider } from "../../../providers/xchange.provider";
import { StateProvider } from "../../../providers/app-state.provider";
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';
import { Keyboard } from '@ionic-native/keyboard';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    templateUrl: "account-payment-page.component.html"
})

export class AccountPaymentPageComponent implements OnInit {

    title: string = "Account Info";
    maxYear: string = new Date().getFullYear() - 17 + "";
    accountDetails: FormGroup;
    address: FormGroup;
    private flow: boolean;
    showLoadDialog: boolean = false;
    pageLoadMsg: string = "We are setting up your payment...";


    constructor(private fb: FormBuilder, private pageNav: PageNavigationProvider,
        private toastCtrl: ToastController,
        private xchange: XChangeProvider,
        private state: StateProvider,
        private route: ActivatedRoute,
        private router: Router,
        private keyboard: Keyboard) {

        this.accountDetails = fb.group({
            accountNumber: [null, Validators.compose([Validators.required])],
            routingNumber: [null, Validators.compose([Validators.required])],
            dob: [null, Validators.compose([Validators.required])],
            termsAccepted: [null, Validators.compose([Validators.required])]
        });

        this.address = fb.group({
            streetAddress: [null, Validators.compose([Validators.required])],
            locality: [null, Validators.compose([Validators.required])],
            region: [null, Validators.compose([Validators.required])],
            postalCode: [null, Validators.compose([Validators.required])]
        });
    }

    ngOnInit(): void {
        this.flow = this.route.snapshot.params['flow'];

        this.state.hasAccountDetails()
            .then((hasAccount: boolean) => {
                if (hasAccount) {
                    return this.state.getAccountDetails();
                }

                return hasAccount
            })
            .then((data: any) => {
                if (data) {
                    this.accountDetails.patchValue(data);
                    this.address.patchValue(data.address);
                }
            })
    }

    gotoTerms(): void {
        if (this.flow) {
            this.router.navigate(['shared/terms']);
        }
        else {
            this.pageNav.navigate('main/drive/terms');
        }

    }

    submitAccountDetails(): void {
        if (this.accountDetails.invalid || this.address.invalid) {
            let toast = this.toastCtrl.create({
                message: 'All information requested is mandatory.',
                duration: 3000,
                position: 'top'
            });
            toast.present();
            return;
        }
        this.showLoadDialog = true;
        let data: any = this.accountDetails.value;
        data.address = this.address.value;
        this.state.getProfile()
            .then(profile => {
                data.profile = profile.id;
                return this.state.getDriverId();
            })
            .then(id => {

                data.id = id;
                return this.xchange.addAccountDetails(data);
            })
            .then((result) => {
                this.state.setAccountDetails(result);
            })
            .then(() => {
                if (this.flow) {
                    this.router.navigate(['shared/auth/driver']);
                    this.keyboard.close();
                }
            })
            .catch(error => console.error(error));
    }

}