import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { XChangeProvider } from '../../../providers/xchange.provider';
import { StateProvider } from '../../../providers/app-state.provider';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';
import { Payment } from '../../../models/payment';
import { Keyboard } from '@ionic-native/keyboard';

declare var braintree: any;

@Component({
    templateUrl: "client-payment-page.component.html"
})


export class ClientPaymentPageComponent implements OnInit {

    title: string = 'Payment Info';
    card_details: Payment = null;
    pageLoadMsg: string = "We are securing the setup...";
    showLoadDialog: boolean = true;
    private isClientCreated = false;
    private flow: boolean;

    constructor(private xchange: XChangeProvider, private appState: StateProvider, private keyboard: Keyboard,
        private route: ActivatedRoute, private router: Router,private pageNav:PageNavigationProvider,
        private ref: ChangeDetectorRef) {

    }

    private initiatePaymentMethod(): void {
        this.xchange.getClientAuth()
            .then(auth => {
                this.createPaymentWindow(auth);
            }, error => {
                console.error(error);
            });
    }

    ngOnInit(): void {
        this.flow = this.route.snapshot.params['flow'];
        this.showLoadDialog = true;
        this.appState.hasPaymentDetails()
            .then(has_entry => {
                if (!has_entry) {
                    return new Promise((resolve) => resolve(false));
                }
                else {
                    return this.appState.getPaymentDetails();
                }
            })
            .then((result: any) => {
                if (result && result.maskedNumber) {
                    this.card_details = result;
                    this.showLoadDialog = false;
                    this.ref.detectChanges();
                }
                else {
                    this.initiatePaymentMethod();
                }
            })
            .catch(error => console.error(error));
    }

    delete(): void {
        this.card_details = null;
        this.showLoadDialog = true;
        this.appState.removePaymentDetails()
            .then(() => {
                this.initiatePaymentMethod();
                if (this.isClientCreated)
                    this.showLoadDialog = false;
            })
            .catch(error => {
                console.error(error);
            })

    }

    private createPaymentMethod(token: string): void {
        this.xchange.createClientPayMethod(token)
            .then((data) => {
                this.card_details = new Payment(data);
                return this.appState.setPaymentDetails(this.card_details);
            })
            .then(() => {
                console.log("Stored payment details");
                this.showLoadDialog = false;
                this.keyboard.close();
                this.ref.detectChanges();

                if (this.flow) {
                    if (this.flow) {
                        this.router.navigate(['shared/auth/guest']);
                        this.keyboard.close();
                    }
                }
            })
            .catch((error) => {
                console.error(error);
            })

    }

    private createPaymentWindow(auth: string): void {

        var form: any = document.querySelector('#checkout-form');
        var submit = document.querySelector('button[type="submit"]');
        var self = this;

        braintree.client.create({
            authorization: auth,
        }, function (clientErr, clientInstance) {
            if (clientErr) {
                return;
            }

            braintree.hostedFields.create({
                client: clientInstance,
                styles: {
                    'input': {
                        'font-size': '14pt'
                    },
                    'input.invalid': {
                        'color': 'red'
                    },
                    'input.valid': {
                        'color': '#333'
                    }
                },
                fields: {
                    number: {
                        selector: '#card-number',
                        placeholder: '4111 1111 1111 1111'
                    },
                    cvv: {
                        selector: '#cvv',
                        placeholder: '123'
                    },
                    expirationDate: {
                        selector: '#expiration-date',
                        placeholder: '10/2019'
                    }
                }
            }, function (hostedFieldsErr, hostedFieldsInstance) {
                if (hostedFieldsErr) {
                    return;
                }
                self.isClientCreated = true;
                submit.removeAttribute('disabled');
                self.showLoadDialog = false;
                self.ref.detectChanges();

                hostedFieldsInstance.on('cardTypeChange', function (event) {
                    // Change card bg depending on card type
                    if (event.cards.length === 1) {
                        var element = document.getElementById('card-image');
                        element.className = event.cards[0].type;

                        console.log('card type' + event.cards[0].type);

                        // Change the CVV length for AmericanExpress cards
                        if (event.cards[0].code.size === 4) {
                            /* hostedFieldsInstance.setAttribute({
                                 field: 'cvv',
                                 attribute: 'placeholder',
                                 value: '1234'
                             });*/
                        }
                    } else {
                        /*hostedFieldsInstance.setAttribute({
                            field: 'cvv',
                            attribute: 'placeholder',
                            value: '123'
                        });*/
                        var element = document.getElementById('card-image');
                        element.className = 'none';

                        console.log('card type no card');
                    }
                });

                form.addEventListener('submit', function (event) {
                    event.preventDefault();

                    hostedFieldsInstance.tokenize(function (tokenizeErr, payload) {
                        if (tokenizeErr) {
                            // Handle error in Hosted Fields tokenization
                            return;
                        }

                        // Put `payload.nonce` into the `payment-method-nonce` input, and then
                        // submit the form. Alternatively, you could send the nonce to your server
                        // with AJAX.                        
                        self.createPaymentMethod(payload.nonce);

                        self.showLoadDialog = false;
                        self.ref.detectChanges();

                    });
                }, false);
            });
        });
    }
}