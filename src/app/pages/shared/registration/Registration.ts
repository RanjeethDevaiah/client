import { IRegistration,IProfile } from '../../../models/iRegistration';


export class Profile implements IProfile {
    first_name: string;
    last_name: string;    
    phone: Number;
    email: string;
    override?:boolean;
    id?:string;
    image?:any;

    constructor(private object?:any){
        if(object && typeof object === "object"){
            this.first_name = object.first_name;
            this.last_name = object.last_name;
            this.phone = object.phone;
            this.email = object.email;
            this.image = object.image;

            if(object.override)
                this.override = object.override;

            if(object.id)
                this.id = object.id;
        }
    }

    isEqual(registration:Profile):boolean{
        var currentProps = Object.getOwnPropertyNames(this);
        var inputProps = Object.getOwnPropertyNames(registration);

        if(currentProps.length != inputProps.length)
            return false;

        for(let i = 0 ; i < currentProps.length ; i++){
            let name = currentProps[i];

            if(!inputProps[name])
                return false;

            if(currentProps[name] !== inputProps[name])
                return false;            
        }

        return true;
    }
}

export class Registration implements IRegistration{
    profile:string;
    clientId:string;   
    secret:string;
    is_guest?:boolean;
    is_driver?:boolean;
    addtionalData?:any;    
}