import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActivatedRoute } from '@angular/router';
import { ToastController, AlertController } from 'ionic-angular';

import { XChangeProvider } from '../../../providers/xchange.provider';
import { StateProvider } from '../../../providers/app-state.provider';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';

import { UUID } from 'angular2-uuid';
import { Profile, Registration } from './Registration';
import { Payment } from '../../../models/payment'

@Component({
    templateUrl: 'registration-page.component.html',
})


export class RegistrationPageComponent implements OnInit {
    details: string = "Personal";
    title: string = "Registration";
    private mode: string;
    private flow: boolean;
    private profile: Profile = new Profile();
    isReadOnly: boolean = false;

    form: FormGroup;
    additionalInfoFrm: FormGroup;
    private cameraOptions: CameraOptions;
    waitForResp:boolean =false;
    profileImg: any = "img/no_avatar.jpg";
    carNames:string[] = ["Toyota","Honda","BMW","Mercedies"];
    carModel:string[] = ["Prius","City","X5","S-Series"];
    carColor:string[] = ["Black","White","Silver","Green"];

    constructor(private route: ActivatedRoute,
        private xchange: XChangeProvider,
        private appState: StateProvider,
        private pgNav: PageNavigationProvider,
        private toastCtrl: ToastController,
        private alertCtrl: AlertController,
        private fb: FormBuilder,
        private camera: Camera,
        private ref:ChangeDetectorRef      
    ) {
        
        //this needs to be here for some how form control to work otherwise type script doesnot compile properly. strange
        this.mode = this.route.snapshot.params['mode'];
        this.flow = this.route.snapshot.params['flow'];

        this.form = fb.group({
            first_name: [null, Validators.compose([Validators.required, Validators.pattern("[a-zA-Z ]*")])],
            last_name: [null, Validators.compose([Validators.required, Validators.pattern("[a-zA-Z ]*")])],
            password: "",
            phone: [null, Validators.compose([Validators.required])],
            email: [null, Validators.compose([Validators.required, Validators.email])]
        });

        if (this.mode === "driver") {

            this.additionalInfoFrm = fb.group({
                details: ["Personal"],
                gender: [null, Validators.compose([Validators.required])],
                ssn: [null, Validators.compose([Validators.required])],
                license: [null, Validators.compose([Validators.required])],
                make: [null, Validators.compose([Validators.required])],
                model: [null, Validators.compose([Validators.required])],
                color: [null, Validators.compose([Validators.required])],
                registration: [null, Validators.compose([Validators.required])],
                isTaxi: [false]
            });
        }

        this.cameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            targetWidth: 160,
            targetHeight: 160,
            saveToPhotoAlbum: false,
            correctOrientation: true
        }
    }

    ngOnInit(): void {        

        this.appState.hasProfile()
            .then(exists => {
                if (exists)
                    return this.appState.getProfile();
                else
                    return null;
            })
            .then((profile: any) => {
                if (profile) {
                    this.profile = new Profile(profile);
                    if (this.profile.image) {
                        this.profileImg = this.profile.image;
                    }

                    this.isReadOnly = true;
                    this.form.patchValue(this.profile);
                }

                if (this.mode === "driver") {
                    return this.appState.hasDriverProfile();
                }
                else {
                    return false;
                }
            })
            .then((hasDriversProfile) => {
                if (hasDriversProfile) {
                    return this.appState.getDriverProfile();
                }
                else {
                    return null;
                }
            })
            .then((result: any) => {
                if (result && result.addtionalData) {
                    this.additionalInfoFrm.patchValue(result.addtionalData);
                }
            });
    }

    showDetails(): boolean {
        return this.mode == "driver";
    }

    private registerGuest(customer: Registration) {
        customer.is_guest = true;
        return this.xchange.registerCustomer(customer)
            .then(result => {
                return this.appState.setGuestId(result);
            })
            .then(() => {
                return this.appState.setGuestProfile(customer);
            })
            .then(() => {
                if(this.flow){
                    this.pgNav.replace('shared/client-payment', [{ flow: this.flow }]);
                }
                this.waitForResp = false;
            })
            .catch(error => {
                let toast = this.toastCtrl.create({
                    message: error.message,
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            });
    }

    private registerDriver(customer: Registration) {
        customer.is_driver = true;
        return this.xchange.registerCustomer(customer)
            .then(result => {
                return this.appState.setDriverId(result);
            })
            .then(() => {
                return this.appState.setDriverProfile(customer);
            })
            .then(() => {
                if(this.flow){
                    this.pgNav.replace('shared/account-payment', [{ flow: this.flow }]);
                }
                this.waitForResp = false;
            })
            .catch(error => {
                let toast = this.toastCtrl.create({
                    message: error.message,
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
            });
    }

    private updateExistingProfile(profile: Profile, addtionalData: any): void {
        this.appState.setProfile(profile)
            .then(() => {
                var registration = this.createRegistration(profile.id)
                if (this.mode === "driver") {
                    registration.addtionalData = addtionalData
                    registration.is_driver = true;
                }
                else {
                    registration.is_guest = true;
                }

                return this.xchange.updateCustomer(registration);
            })
            .then((data: any) => {
                if (this.mode !== "driver") {
                    let payment = new Payment(data);
                    return this.appState.setPaymentDetails(payment);
                }
            })

    }

    checkIfValid(): boolean {
      
        if (this.mode === "driver") {
            return this.form.valid 
            && this.additionalInfoFrm.valid;
        }
        else {
            return this.form.valid
        }
    }

    submitForm(formProfile: Profile): void {
       
        if (!this.checkIfValid()) {

            var toast = this.toastCtrl.create({
                message: `All fields are mandatory in the form. Verify if all infromation has been provided.`,
                showCloseButton: true,
                closeButtonText: 'Ok',
                position: 'middle'
            });            

            toast.present();
            return;
        }       

        if (this.mode === "driver") {
            if (this.profileImg == "img/no_avatar.jpg") {
                var toast = this.toastCtrl.create({
                    message: `Driver profile needs a picture.`,
                    showCloseButton: true,
                    closeButtonText: 'Ok',
                    position: 'middle'
                });

                toast.onDidDismiss(() => {
                    this.clickPicture();
                });

                toast.present();
                return;
            }
        }

        if (this.profile && !this.profile.isEqual(formProfile)) {
            formProfile.id = this.profile.id;
            this.profile = formProfile;
        }
        else {
            this.profile = formProfile;
        }
        this.register(false);
    }

    private register(override?: boolean): Promise<any> {

        if (override)
            this.profile.override = true;

        if (this.mode === "driver") {
            this.profile.image = this.profileImg;
        }
        this.waitForResp = true;
        return new Promise<any>((resolve) => {
            resolve(this.profile);
        })
            .then((profile: Profile) => {
                if (profile && profile.id)
                    return profile;
                else
                    return this.xchange.registerProfile(profile);
            })
            .then((result: any) => {

                if (result.status) {
                    let prompt = this.alertCtrl.create({
                        title: "Profile",
                        message: `Hey. There might be a profile of you already with email ${result.email} with us, do you want us to use it ${result.firstName} ${result.lastName}?`,
                        buttons: [{
                            text: "Go ahead",
                            handler: data => {
                                this.updateExistingProfile(result, this.additionalInfoFrm.value);
                            }
                        }, {
                            text: "Use the one I create",
                            handler: data => {
                                this.register(true);
                            }
                        }]
                    });
                    prompt.present();

                    return { ignore: true }
                }
                else {
                    this.profile.id = result.id;
                    return this.appState.setProfile(this.profile);
                }
            })
            .then((result: any) => {
                if (result.ignore) {
                    return;
                }

                var registration = this.createRegistration(this.profile.id);
                if (this.mode === "driver") {
                    registration.addtionalData = this.additionalInfoFrm.value
                    return this.registerDriver(registration);
                }
                else {
                    return this.registerGuest(registration);
                }
            })
            .catch(error => {
                console.error(error);
            });
    }


    private createRegistration(profile: string): Registration {
        var registration: Registration = {
            profile: profile,
            clientId: UUID.UUID(),
            secret: UUID.UUID()
        };

        return registration;
    }

    clickPicture(): void {
        this.camera.getPicture(this.cameraOptions)
            .then((imageData) => {
                if (imageData) {
                    this.profileImg = 'data:image/jpeg;base64,' + imageData;
                }

            })
            .catch(error => {

            });

    }
}

