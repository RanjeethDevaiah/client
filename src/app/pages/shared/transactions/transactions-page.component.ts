import { Component, AfterViewInit, ChangeDetectorRef } from "@angular/core";
import { StateProvider } from "../../../providers/app-state.provider";
import { XChangeProvider } from "../../../providers/xchange.provider";
import { ToastController } from 'ionic-angular';
import { PageNavigationProvider } from '../../../providers/page-navigation.provider';

@Component({
    templateUrl: "transactions-page.component.html",
})


export class TransactionPageComponent implements AfterViewInit {
    title: string = "History";
    tag: string = "pending";
    color: any = ["#51B3D7", "#EA4055", "#78BB65", "#158BC5", "#EF7B2F", "#2A3E51", "#D02026", "#E45D25", "#15A757"];
    pendingTrx: any;
    completedTrx: any;
    waitForResp:boolean = false;

    constructor(private appState: StateProvider, private xchange: XChangeProvider,
    private pageNav:PageNavigationProvider, private toastCtrl:ToastController, private ref:ChangeDetectorRef) {

    }

    ngAfterViewInit(): void {
        this.loadData();
    }

    private loadData(): void {
        this.appState.getPendingTranscations()
            .then((records: [any]) => {
                if (records.length) {
                    this.pendingTrx = records;
                }
                else {
                    this.tag = "completed";
                }

                return this.appState.getTranscations();
            })
            .then((records: any) => {
                this.completedTrx = records;
            });
    }


    random(index: number): string {

        return this.color[index % 8];
    }

    deleteTag(completed: any): void {

        console.dir(completed);

        this.appState.removeTransactions(completed.trip_id)
            .then(() => {
                this.loadData();
            });

    }

    private getTransactionDetails(pending: any): Promise<any> {
        let data: any = {
            customer: pending.customer,
            tripId: pending.trip_id,
            transactionId: pending.transaction_id,
            totalDistance: pending.totalDistance,
            totalWaitTime: pending.totalWaitTime
        };


        return this.appState.getTransaction(pending.transaction_id)
            .then((result: any) => {
                if (result) {
                    return result.cost;
                }
                return null;
            })
            .then((amount: number) => {
                if (amount) {
                    return amount;
                }
                else {
                    return this.xchange.tripComplete(data.customer, data.tripId, data.totalDistance, data.totalWaitTime);
                }
            })
            .then((result: any) => {
                if (result.amount) {
                    data.amount = result.amount;
                }
                else {
                    data.amount = result;
                }

                delete data.totalDistance;
                delete data.totalWaitTime;

                return data;
            });
    }

    triggerPayment(pending: any): void {
        let payment;
        this.waitForResp = true;
        this.ref.detectChanges();

        this.getTransactionDetails(pending)
            .then((details: any) => {
                payment = details;
                return this.xchange.requestPayment(details);
            })
            .then(() => {
                return this.appState.setTransaction(pending.trip_id, pending.customer, payment.amount, pending.transaction_id);
            })
            
            .then(() => {               
                return this.appState.setTripDetailsProcessed(pending.trip_id);
            })
            .then(() => {                
                this.waitForResp = false;
                this.loadData();                

                let toast = this.toastCtrl.create({
                    message: `Total payable amount is  ${payment.amount}`,
                    showCloseButton: true,
                    closeButtonText: 'Ok',
                    position: 'middle'
                });

                toast.onDidDismiss(() => {
                    this.pageNav.navigate("main/drive/complete");
                });

                toast.present();                
            })
            .catch(error =>{
                this.waitForResp = false;
                console.log(error);
                this.ref.detectChanges();
            })

            
    }
}