import { Component, OnInit, ChangeDetectorRef } from "@angular/core";
import { XChangeProvider } from '../../../providers/xchange.provider';
import { StateProvider } from '../../../providers/app-state.provider';
import { Router } from '@angular/router';

@Component({
    templateUrl: "trip-complete-page.component.html"

})

export class TripCompletePageComponent implements OnInit {

    title: string = "Trip Complete";
    isDriver: boolean;
    tripData: any = {
        dontRecommend: false,
        rate: 3,
        tag:0,
        tagName:""
    };


    constructor(private appState: StateProvider, private xchange: XChangeProvider, private router:Router, private ref:ChangeDetectorRef) {
        this.isDriver = appState.isDriverMode();
    }

    ngOnInit(): void {
        if (this.isDriver) {
            this.appState.getLatestTranscation()
                .then(result => {
                    this.tripData = result;
                    this.tripData.dontRecommend = false;

                    this.ref.detectChanges();
                });

        }
        else {
            this.appState.getLatestTripComplete()
                .then(result => {
                    this.tripData = result;
                    this.tripData.rate = 3;
                    this.tripData.dontRecommend = false;
                    this.tripData.tag = 0;
                    this.tripData.tagName="";

                    this.calculateTipAmount();


                     this.ref.detectChanges();
                });
        }
    }

    private calculateTipAmount(): void {
        var nearest = Math.ceil(this.tripData.amount / 5) * 5;

        this.tripData.tipAmount = {
            first: nearest,
            second: nearest + 5,
            third: nearest + 10
        }
    }

    getRangeColor(): string {
        if (this.tripData.rate >= 3)
            return "secondary";

        if (this.tripData.rate < 3)
            return "danger";
    }

    payTip(tip: number) {
        this.tripData.tip = tip - this.tripData.amount;
    }

    UpdateTripComplete(): void {

        if (this.isDriver) {
            this.xchange.sendCustomerFeedback(this.tripData.customer, this.tripData.dontRecommend)
                .then(() => {
                    this.router.navigate(['main/drive']);     
                });
        }
        else {
            this.xchange.requestTipsPayment(this.tripData.driver, this.tripData.trip_id, this.tripData.tip)
                .then(() => {
                    return this.appState.updateTipInformation(this.tripData.trip_id, this.tripData.tip);
                })
                .then(() => {
                    return this.xchange.sendDriverFeedback(this.tripData.driver,this.tripData.trip_id,this.tripData.rate,this.tripData.dontRecommend)
                })
                .then(() => {
                    if(this.tripData.tag){
                        return this.appState.setLocationTag(this.tripData.trip_id,this.tripData.tag,this.tripData.tagName);
                    }
                })
                .then(() => {
                    return this.appState.removeTripLocation(this.tripData.trip_id);
                })
                .then(()=> {                 
                     this.router.navigate(['main/book']);                    
                });
        }

    }


    tagLocation(type:number):void{
        this.tripData.tag = type;
        if(type === 1){
            this.tripData.tagName = "Home";
        }

        if(type === 2){
            this.tripData.tagName = "Fav";
        }      
    }
}

