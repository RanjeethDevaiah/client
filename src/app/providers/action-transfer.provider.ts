import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/Subject';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class ActionTransfer {
    private initializeMapObserver: ReplaySubject<void> = new ReplaySubject<void>();    
    private componentsLoadedObserver:ReplaySubject<void> = new ReplaySubject<void>();
    private menuFavObserver:Subject<any> = new Subject<any>();


    intializedMap(): void {
        this.initializeMapObserver.next(null);
    }

    onMapInitialized(): Subject<void> {
        return this.initializeMapObserver;
    }    

    componentsInitialized(global:any): any{
        this.componentsLoadedObserver.next(global);
    }

    onComponentLoaded():ReplaySubject<any>{
        return this.componentsLoadedObserver;
    }

    onFavSelected():Subject<any>{
        return this.menuFavObserver;
    }

    invokeFavSelected(fav:any):void{
        this.menuFavObserver.next(fav);
    }

}