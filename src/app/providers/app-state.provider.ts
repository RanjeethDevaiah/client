import { Injectable } from "@angular/core";
import { SQLiteObject } from '@ionic-native/sqlite';
import { ActionTransfer } from "./action-transfer.provider";

import { IPlace } from "../models/iPlace";


@Injectable()
export class StateProvider {

    private db: SQLiteObject;
    private is_driver: boolean;
    private driver_id: string = "driver_id";
    private guest_id: string = "guest_id";
    private guest_profile: string = "guest_profile";
    private driver_profile: string = "driver_profile";
    private payment_mode: string = "payment_mode";
    private account_mode: string = "account_mode";
    private profile: string = "profile";
    private guest_state: string = "guest_state";
    private driver_state: string = "driver_state";

    private tempToken: any = "";

    constructor(private action: ActionTransfer) {
        action.onComponentLoaded()
            .subscribe((global: any) => {
                this.db = global.db;
            })
    }

    hasProfile(): Promise<boolean> {
        return this.hasConfigValue(this.profile);
    }

    getProfile(): Promise<any> {
        return this.getConfigValue(this.profile);
    }

    getToken(): any {
        return this.tempToken;
    }

    setToken(token: any): void {
        this.tempToken = token;
    }

    setProfile(data: any): Promise<void> {
        return this.setConfig(this.profile, JSON.stringify(data));
    }

    hasGuestProfile(): Promise<boolean> {
        return this.hasConfigValue(this.guest_profile);
    }

    hasDriverProfile(): Promise<boolean> {
        return this.hasConfigValue(this.driver_profile);
    }

    getGuestProfile(): Promise<any> {
        return this.getConfigValue(this.guest_profile);
    }

    getDriverProfile(): Promise<any> {
        return this.getConfigValue(this.driver_profile);
    }

    setGuestProfile(data: any): Promise<void> {
        return this.setConfig(this.guest_profile, JSON.stringify(data));
    }
    setDriverProfile(data: any): Promise<void> {
        return this.setConfig(this.driver_profile, JSON.stringify(data));
    }

    getType(): Promise<Number> {
        return this.getConfigValue(this.driver_profile)
            .then((profile: any) => {
                return (profile.addtionalData.isTaxi) ? 1 : 2;
            })
    }

    hasGuestState(): Promise<boolean> {
        return this.hasConfigValue(this.guest_state);
    }
    getGuestState(): Promise<any> {
        return this.getConfigValue(this.guest_state);
    }
    setGuestState(data: any): Promise<void> {
        return this.setConfig(this.guest_state, data);
    }
    deleteGuestState(): Promise<void> {
        return this.removeConfig(this.guest_state);
    }

    hasDriverState(): Promise<boolean> {
        return this.hasConfigValue(this.driver_state);
    }
    getDriverState(): Promise<any> {
        return this.getConfigValue(this.driver_state);
    }
    setDriverState(data: any): Promise<void> {
        return this.setConfig(this.driver_state, data);
    }
    deleteDriverState(): Promise<void> {
        return this.removeConfig(this.driver_state);
    }

    setDriverMode(): void {
        this.is_driver = true;
    }

    setGuestMode(): void {
        this.is_driver = false;;
    }

    isDriverMode(): boolean {
        return this.is_driver;
    }

    setDriverId(id: string): Promise<void> {
        return this.setConfig(this.driver_id, id);
    }

    getDriverId(): Promise<string> {
        return this.getConfigValue(this.driver_id);
    }

    setPaymentDetails(details: any): Promise<any> {
        return this.setConfig(this.payment_mode, JSON.stringify(details));
    }

    setAccountDetails(details: any): Promise<any> {
        return this.setConfig(this.account_mode, JSON.stringify(details));
    }

    getPaymentDetails(): Promise<any> {
        return this.getConfigValue(this.payment_mode);
    }

    getAccountDetails(): Promise<any> {
        return this.getConfigValue(this.account_mode);
    }
    hasPaymentDetails(): Promise<boolean> {
        return this.hasConfigValue(this.payment_mode);
    }

    hasAccountDetails(): Promise<boolean> {
        return this.hasConfigValue(this.account_mode);
    }

    removePaymentDetails(): Promise<void> {
        return this.removeConfig(this.payment_mode);
    }

    setGuestId(id: string): Promise<void> {
        return this.setConfig(this.guest_id, id);
    }

    getGuestId(): Promise<string> {
        return this.getConfigValue(this.guest_id);
    }

    private hasConfigValue(key: string): Promise<boolean> {
        
        return this.db.executeSql("SELECT [value] FROM config WHERE key=?", [key])
            .then(resultSet => {
                let value = resultSet.rows.length > 0;
                return value;
            })
            .catch((error) => {
                throw key + " " + error;
            })
    }

    private getConfigValue(key: string): Promise<any> {
        return this.db.executeSql("SELECT [value] FROM config WHERE key=?", [key])
            .then(resultSet => {
                let value = resultSet.rows.item(0).value;
                return value;
            })
            .then(value => {
                try {
                    return JSON.parse(value);
                }
                catch (e) {
                    return value;
                }
            });
    }

    private setConfig(key: string, value: any): Promise<void> {
        var dbVal = value + '';
        return this.db.executeSql("INSERT OR REPLACE INTO config (key,value) VALUES(?,?)", [key, dbVal]);
    }

    private removeConfig(key: string): Promise<void> {
        return this.db.executeSql("DELETE FROM config  WHERE key = ?", [key]);
    }

    setTransaction(tripId: string, customerId: string, amount: number, transactionId: string): Promise<void> {
        return this.db.executeSql("INSERT OR REPLACE INTO transactions (trip_id, customer, amount, transaction_id, updated) VALUES(?,?,?,?,?)",
            [tripId, customerId, amount, transactionId, new Date()])
            .then(() => {
                return;
            })
    }

    setTripDetails(tripId: string, customerId: string, totalAmount: number, totalWaitTime: Number, transaction:string): Promise<void> {
        return this.db.executeSql("INSERT OR REPLACE INTO trip_details (trip_id, customer, totalDistance, totalWaitTime, transaction_id, updated) VALUES(?,?,?,?,?,?)",
            [tripId, customerId, totalAmount, totalWaitTime, transaction, new Date()])
            .then(() => {
                return;
            })
    }
    setTripDetailsProcessed(tripId: string): Promise<void> {
        return this.db.executeSql("DELETE FROM trip_details WHERE  trip_id = ?", [tripId])
            .then(() => {
                return;
            })
    }

    removeTransactions(tripId: string): Promise<void> {
        return this.db.executeSql("DELETE FROM transactions  WHERE trip_id = ?", [tripId])
            .then(() => {
                console.log("Removed transaction with trip id" + tripId);
                return;
            })
    }

    getTranscations(): Promise<any[]> {
        return this.db.executeSql("SELECT *FROM transactions ORDER BY datetime(updated) DESC", [])
            .then(resultSet => {
                let rows = [];

                for (let i = 0; i < resultSet.rows.length; i++) {
                    rows.push(resultSet.rows.item(i));
                }
                return rows;
            });
    }

    getPendingTranscations(): Promise<any[]> {
        return this.db.executeSql("SELECT *FROM trip_details WHERE processed = ?", [0])
            .then(resultSet => {
                let rows = [];

                for (let i = 0; i < resultSet.rows.length; i++) {
                    rows.push(resultSet.rows.item(i));
                }
                return rows;
            });
    }

    getTransaction(transaction:any):Promise<any>{
        return this.db.executeSql("SELECT *FROM transactions WHERE transaction_id = ?   ORDER BY datetime(updated) DESC LIMIT 1", [transaction])
        .then(resultSet => {
            if(resultSet.rows.length){
                return resultSet.rows.item(0);
            }           
           
        });
    }

    getLatestTranscation(): Promise<any> {
        return this.db.executeSql("SELECT *FROM transactions  ORDER BY datetime(updated) DESC LIMIT 1", [])
            .then(resultSet => {
                return resultSet.rows.item(0);
            });
    }

    setTripComplete(trip_id: string, driver: string, amount: number) {
        return this.db.executeSql("INSERT INTO trip_complete (trip_id,driver,amount,updated) VALUES (?,?,?,?)", [trip_id, driver, amount, new Date()])
            .then(() => {
                return;
            })
    }

    updateTipInformation(trip_id: string, tip: number) {
        return this.db.executeSql("UPDATE trip_complete SET tip = ?, processed = 1 WHERE trip_id = ?", [tip, trip_id])
            .then(() => {
                return;
            })
    }

    getLatestTripComplete(): Promise<any> {
        return this.db.executeSql("SELECT * FROM trip_complete WHERE processed = 0 ORDER BY datetime(updated) DESC LIMIT 1", [])
            .then(resultSet => {
                return resultSet.rows.item(0);
            });
    }

    hasLatestTripComplete(): Promise<any> {
        return this.db.executeSql("SELECT * FROM trip_complete WHERE processed = 0 ORDER BY datetime(updated) DESC LIMIT 1", [])
            .then((resultSet: any) => {
                let val = resultSet.rows.length > 0;

                return val;
            });

    }


    setTripLocation(tripId: string, start:IPlace, end:IPlace): Promise<void> {
        return this.db.executeSql("INSERT INTO trip_location (trip_id, start_loc, end_loc, start_address, end_address, start_geo, end_geo, updated) VALUES (?,?,?,?,?,?,?,?)", [
            tripId,
            JSON.stringify(start.location),
            JSON.stringify(end.location),
            start.address,
            end.address,
            JSON.stringify(start.geo),
            JSON.stringify(end.geo),
            new Date()
        ])
            .then(() => {
                return;
            });
    }

    removeTripLocation(tripId:string):Promise<boolean>{
        return this.db.executeSql("DELETE FROM trip_location WHERE trip_id = ?",[tripId])
        .then(()=>{
            return true;
        })
    }

    getTripLocation(tripId: string): Promise<any> {
        return this.db.executeSql("SELECT *FROM trip_location WHERE trip_id = ? ORDER BY datetime(updated) DESC LIMIT 1", [tripId])
            .then((resultSet: any) => {
                let data = resultSet.rows.item(0);
                data.start_loc = JSON.parse(data.start_loc);
                data.end_loc = JSON.parse(data.end_loc);

                return data;
            });

    }

    setLocationTag(tripId: string, type: number, tagName: string): Promise<any> {
        return this.getTripLocation(tripId)
            .then((data: any) => {

                return this.db.executeSql("INSERT INTO favourites (tag_name, end_loc, end_address, end_geo ,type, updated) VALUES (?,?,?,?,?,?)",
                    [tagName, JSON.stringify(data.end_loc), JSON.stringify(data.end_address), JSON.stringify(data.end_geo),type, new Date()]);
            });
    }

    getLocationTag(): Promise<any> {
        return this.db.executeSql("SELECT rowid, tag_name, end_loc, end_address, end_geo, type FROM favourites", [])
            .then((resultSet: any) => {
                let rows = {
                    1: [],
                    2: []
                };

                for (let i = 0; i < resultSet.rows.length; i++) {
                    var data = resultSet.rows.item(i);
                    data.end_loc = JSON.parse(data.end_loc);
                    data.end_address = JSON.parse(data.end_address);
                    data.end_geo = JSON.parse(data.end_geo);
                    rows[data.type].push(data);
                }
                return rows;
            });

    }

    removeLocationTag(id: any): Promise<boolean> {
        return this.db.executeSql("DELETE FROM favourites WHERE rowid = ?", [id])
            .then(() => {
                return true;
            });
    }

}