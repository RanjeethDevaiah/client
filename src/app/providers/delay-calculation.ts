import { Injectable } from '@angular/core';

import { ILocation } from "../models/iLocation";
import { Subject } from 'rxjs/subject';

@Injectable()
export class DelayCalculation {
    private prevValue: any;
    private initDate: Date;
    private speedArray: number[] = [];
    private delaySubject: Subject<number> = new Subject<number>();
    private map:any;

    set mapObject(mapObj:any){
        this.map = mapObj;
    }

    subscribeToDelay(): Subject<number> {
        return this.delaySubject;
    }

    measureDelay(location: ILocation, date: number) {
        if (this.prevValue) {
            var delay, timeDiff;


            if ((delay = (date - this.prevValue.date) / 1000) < 1) {
                return;
            }


            let speed = this.map.calculateSpeed(
                this.prevValue.location.latitude,
                this.prevValue.location.longitude,
                location.latitude,
                location.longitude,
                delay);

            this.speedArray.push(Math.round(speed));

            this.prevValue = {
                location: location,
                date: date
            }

            if ((timeDiff = this.calculateDifferenceInSec(new Date(), this.initDate)) > 29) {
                let total = 0;

                for (let i = 0; i < this.speedArray.length; i++) {
                    total += this.speedArray[i];
                }

                if (total) {
                    let avg = Math.round(total / this.speedArray.length);

                    console.log("The avg speed is "+ avg + " kms/h");

                    if (avg < 5) {
                        // if the time is below 5 mph, the taxi is considered to be not moving.
                        this.addToDelay(timeDiff);
                    }

                }
                else {
                    this.addToDelay(timeDiff);

                    console.log("The avg speed is 0 kms/h");
                }

               this.reset();
            }
        }
        else {
            this.prevValue = {
                location: location,
                date: date
            }

            this.initDate = new Date();
        }

    }

    private calculateDifferenceInSec(max: Date, min: Date) {

        let diff = max.getTime() - min.getTime();
        diff = diff / 1000;

        

        return diff;
    }

    private addToDelay(delay: number): void {
        this.delaySubject.next(delay);
    }

    reset():void{
        this.speedArray = [];
        this.prevValue = null;
        this.initDate = null;
    }
}