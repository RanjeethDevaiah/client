import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { IEvent } from '../models/IEvent';
import { StateProvider } from './app-state.provider';
import { ILocation } from '../models/iLocation';


declare var io: any

export class Event implements IEvent {

    constructor(public type: string, public data: any) { }
}


@Injectable()
export class EventProvider {
    //http://67.161.49.168/
    private url: string = "http://192.168.0.52:9000";
    private socket: any;

    private rideReceived: Subject<Event> = new Subject<Event>();
    private driverRideCancelled: Subject<Event> = new Subject<Event>();
    private customerRideCancelled: Subject<Event> = new Subject<Event>();
    private proceedToLocation: Subject<Event> = new Subject<Event>();
    private reachedLocation: Subject<Event> = new Subject<Event>();
    private tripStarted: Subject<Event> = new Subject<Event>();
    private tripComplete: Subject<Event> = new Subject<Event>();
    private trackRideLocation: Subject<Event> = new Subject<Event>();

    private connectionOserver: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

    private userId: string;
    private clientId: string;

    private isInit: boolean = false;


    constructor(private appState: StateProvider) {
    }

    //RideAtLocation_received
    connect(): Promise<boolean> {
        return ((this.appState.isDriverMode()) ? this.appState.getDriverId() : this.appState.getGuestId())
            .then(id => {
                this.userId = id;
                return (this.appState.isDriverMode()) ? this.appState.getDriverProfile() : this.appState.getGuestProfile();
            })
            .then((profile: any) => {
                var self = this;
                return new Promise((resolve, reject) => {
                    self.clientId = profile.clientId;

                    self.socket = io(self.url, {
                        reconnection: true,
                        reconnectionDelay: 1000,
                        reconnectionDelayMax: 5000,
                        reconnectionAttempts: Infinity,
                        query: {
                            clientId: self.clientId,
                            user: self.userId,
                            token: self.appState.getToken()
                        },
                        transports: ['websocket', 'polling']
                    });

                    self.socket.on('connect', () => {
                        if (this.isInit) {
                            return;
                        }
                        self.listenToRideRequestEvent();
                        self.listenToCustomerRideCancellationEvent();
                        self.listenToDriverRideCancellationEvent();
                        self.listenToProceedToLocatiion();
                        self.listenToReachedLocatiion();
                        self.listenToTripStarted();
                        self.listenToTripComplete();
                        self.listenToTrackRideLocation();
                        this.isInit = true;
                        this.connectionOserver.next(true);
                        console.info("event connected");
                        resolve();
                    });

                    self.socket.on('disconnect', (message) => {
                        console.error("event disconnected - " + message);
                        this.connectionOserver.next(false);
                    });

                    self.socket.on('reconnect_attempt', () => {
                        self.socket.io.opts.query = {
                          token: self.appState.getToken()
                        }
                      });

                    self.socket.on('reconnect', (message) => {
                        console.info("event reconnected");
                        this.connectionOserver.next(true);
                    });

                });
            })
            .then(() => {
                return true;
            });
    }

    isInitialized(): boolean {
        return this.isInit;
    }


    subscribeToConnectionStatus(): BehaviorSubject<boolean> {
        return this.connectionOserver;
    }

    private listenToRideRequestEvent(): void {
        let self = this;

        this.socket.on('RideRequest_received', (message, callback) => {
            console.log('Ride request received.');
            this.rideReceived.next(new Event("RideRequest", message));

            callback({
                event: "RideRequest_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToRideRequestEvent(): Subject<Event> {
        return this.rideReceived;
    }

    private listenToCustomerRideCancellationEvent(): void {
        let self = this;

        this.socket.on('CustomerRideCancelled_received', (message, callback) => {
            console.log("Customer ride cancellation received");
            this.customerRideCancelled.next(new Event("CustomerRideCancelled", message));

            callback({
                event: "CustomerRideCancelled_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToCustomerRideCancellationEvent(): Subject<Event> {
        return this.customerRideCancelled;
    }

    private listenToDriverRideCancellationEvent(): void {
        let self = this;

        this.socket.on('DriverRideCancelled_received', (message, callback) => {
            this.driverRideCancelled.next(new Event("DriverRideCancelled", message));

            callback({
                event: "DriverRideCancelled_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToDriverRideCancellationEvent(): Subject<Event> {
        return this.driverRideCancelled;
    }

    private listenToProceedToLocatiion(): void {
        let self = this;
        this.socket.on('ProceedToLocation_received', (message, callback) => {
            this.proceedToLocation.next(new Event("ProceedToLocation", message));
            callback({
                event: "ProceedToLocation_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToProceedToLocationEvent(): Subject<Event> {
        return this.proceedToLocation;
    }

    private listenToReachedLocatiion(): void {
        let self = this;

        this.socket.on('RideAtLocation_received', (message, callback) => {
            this.reachedLocation.next(new Event("RideAtLocation", message));
            callback({
                event: "RideAtLocation_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToReachedLocatiion(): Subject<Event> {
        return this.reachedLocation;
    }

    private listenToTripStarted(): void {
        let self = this;

        this.socket.on('TripStarted_received', (message, callback) => {
            console.log("Trip started received");
            this.tripStarted.next(new Event("TripStarted", message));
            callback({
                event: "TripStarted_received",
                id: self.userId,
                clientId: self.clientId
            });
        });
    }
    subscribeToTripStarted(): Subject<Event> {
        return this.tripStarted;
    }

    private listenToTripComplete(): void {
        let self = this;

        this.socket.on('TripComplete_received', (message, callback) => {
            console.log("Trip complete received");
            this.tripComplete.next(new Event("TripComplete", message));

            callback({
                event: "TripComplete_received",
                id: self.userId,
                clientId: self.clientId
            });

        });
    }

    subscribeToTrackRideLocation(): Subject<Event> {
        return this.trackRideLocation;
    }

    private listenToTrackRideLocation(): void {
        this.socket.on('TrackRideLocation_received', message => {
            console.log("Track location Received");
            this.trackRideLocation.next(new Event("TrackRideLocation", message));
        });
    }

    subscribeToTripComplete(): Subject<Event> {
        return this.tripComplete
    }

    sendCurrentLocation(data: ILocation): void {
        this.socket.emit("CurrentLocation_Send", data);
    }

    trackCurrentLocation(data: any) {
        this.socket.emit("TrackLocation_Send", data);
    }
    trackRoute(data: any): Promise<void> {
        return this.emitEventWithAck("waypoint_Send", data);
    }

    private emitEventWithAck(event: string, data: any): Promise<void> {
        return new Promise<void>((resolve, reject) => {

            var subscription: any = {};

            subscription.handle = this.connectionOserver.subscribe(connected => {
                if (!connected) {
                    subscription.handle.unsubscribe();
                    reject("Client not connected");
                }
            });

            this.socket.emit(event, data, (ack) => {
                try {

                    subscription.handle.unsubscribe();

                    if (ack.event == event && ack.clientId == this.clientId) {
                        resolve();
                    }
                    else {
                        reject("Sync error");
                    }
                }
                catch (error) {
                    reject(error);
                }
            });
        })

    }
}
