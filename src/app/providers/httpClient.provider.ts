import { Http, Headers, RequestOptions } from "@angular/http";
import { Injectable } from "@angular/core";
import { StateProvider } from "./app-state.provider";


@Injectable()
export class HttpClientProvider {

    private baseUrl: string = "http://192.168.0.52:9000/api";

    constructor(private http: Http, private appState: StateProvider) {
    }

    private getUrl(uri: string): string {
        return `${this.baseUrl}/${uri}`;
    }


    get(uri: string): Promise<any> {
        return this.getHeader()
            .then((headers: RequestOptions) => {
                return this.http.get(this.getUrl(uri), headers).toPromise();
            })
            .then(data => {
                return data.json().message;
            })
            .catch(error => {
                throw JSON.parse(error.text())
            })
    }

    post(uri: string, data: any): Promise<any> {
        return this.getHeader()
            .then((headers: RequestOptions) => {
                return this.http.post(this.getUrl(uri), data, headers).toPromise();
            })
            .then(data => {
                return data.json().message;
            })
            .catch(error => {
                throw JSON.parse(error.text())
            })
    }

    put(uri: string, data: any): Promise<any> {
        return this.getHeader()
            .then((headers: RequestOptions) => {
                return this.http.put(this.getUrl(uri), data, headers).toPromise();
            })
            .then(data => {
                return data.json().message;
            })
            .catch(error => {
                throw JSON.parse(error.text());
            })
    }

    delete(uri: string): Promise<any> {
        return this.getHeader()
            .then((headers: RequestOptions) => {
                return this.http.delete(this.getUrl(uri), headers).toPromise();
            })
            .then(data => {
                return data.json().message;
            })
            .catch(error => {
               throw JSON.parse(error.text());
            })
    }

    private getHeader(): Promise<RequestOptions> {

        let headers: Headers =  new Headers()
        headers.append("Content-Type","application/json");
       
        headers.append("Client-Token", this.appState.getToken());       
         
        return ((this.appState.isDriverMode()) ? this.appState.hasDriverProfile() : this.appState.hasGuestProfile())
            .then((hasProfile: boolean) => {
                if (!hasProfile) {
                    return hasProfile;
                }
                return (this.appState.isDriverMode()) ? this.appState.getDriverProfile()
                    : this.appState.getGuestProfile();
            })
            .then((result: any) => {
                if (result) {
                    headers.append("Client-ID", result.clientId );
                }
                
                return new RequestOptions({ headers: headers });
            });
    }

}