import { Injectable } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';;
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation';

import { Subject } from 'rxjs/subject';
import { Subscription } from 'rxjs/Subscription';

import { ILocation } from '../models/iLocation';
import { Location } from './location';


@Injectable()
export class LocationTracker {

  private backgroundGeolocationObservable: any;
  private locationObserver = new Subject<ILocation>();
  private trackingStarted: boolean = false;
  private watchSubscription:Subscription;
  private backgroundSubscription:Subscription;
  private watch: any;
  private watchOptions: Object = {
    frequency: 3000,
    enableHighAccuracy: true,
    accuracy: 10
  }

  constructor(private geolocation: Geolocation, private backgroundGeolocation: BackgroundGeolocation) {

    let config: BackgroundGeolocationConfig = {
      desiredAccuracy: 0,
      stationaryRadius: 20,
      distanceFilter: 10,
      interval: 3000,
      debug: false, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
    };

    this.backgroundGeolocationObservable = this.backgroundGeolocation.configure(config);

    this.watch = this.geolocation.watchPosition(this.watchOptions);

  }


  startTracking(): void {

    if (this.trackingStarted)
      return;

    var self = this;

    this.backgroundSubscription =this.backgroundGeolocationObservable.subscribe((location: BackgroundGeolocationResponse) => {

      self.propogateLocation({ latitude: location.latitude, longitude: location.longitude });

      // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
      // and the background-task may be completed.  You must do this regardless if your HTTP request is successful or not.
      // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
      this.backgroundGeolocation.finish(); // FOR IOS ONLY

    }, error => {
      console.error('Background location observer error' + error);
    });

    this.backgroundGeolocation.start();

    this.watchSubscription = this.watch.filter((p: any) => p.code === undefined).subscribe((position: Geoposition) => {
      self.propogateLocation({ latitude: position.coords.latitude, longitude: position.coords.longitude });
    }, error => {
      console.error('Geo location error while retrieving data ', error);
    });

    this.trackingStarted = true;
  }

  private propogateLocation(location:ILocation):void{
     this.locationObserver.next({ latitude: location.latitude, longitude: location.longitude });
  }

  subscribeToLocationTracking(): Subject<ILocation> {
    return this.locationObserver;
  }

  stopTracking(subscription?: Subscription): void {
    this.trackingStarted = false;
    
    if(this.backgroundSubscription)
      this.backgroundSubscription.unsubscribe();

    this.backgroundGeolocation.stop();
    
    if(this.watchSubscription)
      this.watchSubscription.unsubscribe();
    
    if (subscription)
      subscription.unsubscribe();

  }

  getCurrentLocation(): Promise<ILocation> {
    return this.geolocation.getCurrentPosition(this.watchOptions)
      .then(position => {
        return (new Location(position.coords.latitude, position.coords.longitude));
      });
  }
}
