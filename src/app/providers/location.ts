import { ILocation } from '../models/iLocation';

export class Location implements ILocation {

    constructor(public latitude: number,
        public longitude: number) { }
}