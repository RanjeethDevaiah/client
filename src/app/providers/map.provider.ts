import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/subject';
import { SQLiteObject } from '@ionic-native/sqlite';
import { ScriptLoaderService } from './script-loader.service';
import { ILocation } from '../models/iLocation';
import { IPlace } from '../models/iPlace';
import { IReverseGeoLocation } from '../models/iReverseGeo';
import { IDistanceMatrix } from '../models/iDistanceMatrix';
import { ITypeLocation } from '../models/ITypeLocation';
import { ActionTransfer } from "./action-transfer.provider";

import { DelayCalculation } from "./delay-calculation";

declare var window: any;
declare var Maps: any;


@Injectable()
export class MapService {
    private apiKey: string = 'AIzaSyBy_P93F58iOebAPY8piEHjDpZAXarhRgI';
    private map: any;
    private url: string = `https://maps.googleapis.com/maps/api/js?v=3&key=${this.apiKey}&callback=initialized&libraries=geometry,places`;

    private db: SQLiteObject;
    private prevLatLang: ILocation;
    private placeChanged = new Subject<IPlace>();
    private mapClicked = new Subject<any>();
    private currentChanged = new Subject<ILocation>();


    constructor(private loader: ScriptLoaderService, private action: ActionTransfer, private delayCalculation: DelayCalculation) {
        action.onComponentLoaded()
            .subscribe((global: any) => {
                this.db = global.db;
                this.map = new Maps();
                this.delayCalculation.mapObject = this.map;
            });
    }

    loadMap(canvas: HTMLElement): Promise<void> {

        return new Promise<void>((resolve, reject) => {
            this.loader.loadScript(this.url, this.onMapFileLoaded);
            window.initialized = () => {
                this.map.init(canvas);
                this.onPlaceChange();
                this.onMapClicked();
                this.onCurrentLocationChange();

                resolve();
            }
        });
    }

    onMapFileLoaded(): void {
        console.log('Map loaded proceed to intialize');
    }

    initLocation(location: ILocation, ignore?: boolean): void {
        this.map.setCurrentLocation(location, null, ignore);
    }

    setFromSearchInput(from: HTMLInputElement): void {
        this.map.setFromSeachBox(from);

    }

    private onPlaceChange(): void {
        this.map.onPlaceChanged()
            .subscribe(data => {
                this.placeChanged.next({ location: data.location, address: data.address.formatted_address });
            });

    }

    private onCurrentLocationChange(): void {
        this.map.subscribeCurrentLocationChange()
            .subscribe(location => {
                this.currentChanged.next(location);
            });
    }


    onCurrentLocationEvent(): Subject<ILocation> {
        return this.currentChanged;
    }

    onPlaceChangeEvent(): Subject<IPlace> {
        return this.placeChanged;

    }

    setToSearchInput(to: HTMLElement): void {
        this.map.setToSeachBox(to);
    }

    setToSearchInput2(to: Element): void {
        this.map.setToSeachBox2(to);
    }

    getApproximateDistance(): Promise<number> {
        return this.map.approxTotalDistance();
    }

    getDestinations(): IPlace[] {
        this.map.state;
        var places = [];

        if (this.map.state.current.address) {
            places.push({
                address: this.map.state.current.address.formatted_address,
                location: { latitude: this.map.state.current.latitude, longitude: this.map.state.current.longitude },
                geo: { address: this.map.state.current.address.formatted_address, city: this.map.state.current.address.address_components[2].long_name }
            });
            if (this.map.state.end.address) {
                let destination: any = {                   
                    location: { latitude: this.map.state.end.latitude, longitude: this.map.state.end.longitude }
                }

                if (this.map.state.end.address.formatted_address) {
                    destination.address = this.map.state.end.address.formatted_address,
                    destination.geo = { address: this.map.state.end.address.formatted_address, city: this.map.state.end.address.address_components[2].long_name }
                }
                else {
                    destination.address = this.map.state.end.address.address,
                    destination.geo = this.map.state.end.address;
                }

                places.push(destination);
            }
        }
        return places;
    }

    startNavigation(location: ILocation, type: number): void {
        this.map.setNavHeading(location, type);
    }
    selfNavigation(location: ILocation, type: number): void {
        this.map.setNavHeadingSelf(location, type);
    }

    startInitialize(): void {
        this.prevLatLang = null;
        this.map.initializeDistanceCaluclation();
    }

    addLocationToDistance(tripId: string, location: ILocation, time: number): Promise<void> {

        try {

            this.delayCalculation.measureDelay(location, time);

            if (this.prevLatLang && this.prevLatLang.latitude == location.latitude && this.prevLatLang.longitude == location.longitude) {
                return Promise.resolve();
            }

            return this.db.executeSql("INSERT INTO way_points (trip_id, location, updated) VALUES(?,?,?)", [tripId,
                JSON.stringify([location.latitude, location.longitude]), new Date()])
                .then(() => {
                    this.map.nextTraveledLocation(location);
                    this.prevLatLang = location;


                });
        }
        catch (error) {
            console.error("Error in waypoint");

            return Promise.reject(error);
        }
    }

    getTotalDistance(): number {
        return this.map.getTotalDistance();
    }

    stopNavigation(): void {
        this.map.resetNavHeading();
    }

    private onMapClicked(): void {
        this.map.subscribeOnMapClicked()
            .subscribe(any => this.mapClicked.next(any));
    }

    listenToMapClick(): Subject<any> {
        return this.mapClicked;
    }

    reverseGeoLocation(location: ILocation): Promise<IReverseGeoLocation> {
        return this.map.reverseGeocoding(location)
            .then(address => {
                return { address: address.formatted_address, city: address.address_components[2].long_name, location: location };
            })
    }

    caluculateDistanceFromCustomer(start: ILocation, end: ILocation): Promise<IDistanceMatrix> {
        return this.map.getDistanceMatrix(start, end);
    }

    setRidesToLocation(typeLoc: ITypeLocation): void {
        this.map.setRandomHeadingMarker(typeLoc);
    }

    clearRidesToLocation(): void {
        this.map.clearRandomMarkers();
    }

    addDelayPoint(overlay: HTMLElement): void {
        this.map.setMarkerOverlay(overlay);
    }

    removeDelayPoint(): void {
        this.map.removeMarkerOverlay();
    }

    setStartLocation(location: ILocation): void {
        this.map.setCurrentLocation(location);
    }

    setEndLocation(location: ILocation): void {
        this.map.setEndLocation(location);
    }

    setEndPlace(place: IPlace): void {
        this.map.setEndLocation(place.location, place.geo);
    }

    clearLocationMarker(): void {
        this.map.clearLocationMarkers();
    }

    setToCenter(): void {
        this.map.setStart2Center();
    }

    resetToInitial(location: ILocation, ignore?: boolean): void {
        this.map.resetToInitial(location, ignore);
    }
}