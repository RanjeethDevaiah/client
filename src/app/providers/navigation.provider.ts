import { Injectable } from '@angular/core';
import { IPlace } from '../models/iPlace'
import { LaunchNavigator, LaunchNavigatorOptions } from '@ionic-native/launch-navigator';
declare var launchnavigator;

@Injectable()
export class NavigationService {

    constructor(private launchNav: LaunchNavigator) { }


    startNavigation(start: IPlace, stop: IPlace): void {

        

        console.log('Navigation func');

        let options: LaunchNavigatorOptions = {
            // start: start.location.latitude+ ", "+start.location.longitude,
            transportMode: this.launchNav.TRANSPORT_MODE.DRIVING
        }

       

        if (launchnavigator)
            options.launchMode = launchnavigator.LAUNCH_MODE.TURN_BY_TURN;

        this.launchNav.isAppAvailable(this.launchNav.APP.GOOGLE_MAPS)
        .then(
            isAvailable => {
                if (isAvailable)
                    options.app = this.launchNav.APP.GOOGLE_MAPS;
                else
                     options.app = this.launchNav.APP.USER_SELECT;

                this.launchNav.navigate([stop.location.latitude, stop.location.longitude],
                options).then(
                success => console.log('Launched navigator'),
                error => console.log('Error launching navigator: ' + error)
                );
            }
        ).catch(error => {
            error => {
                console.error('While finding installed applications ' + error);
                
                this.launchNav.navigate([stop.location.latitude, stop.location.longitude],
                options).then(
                success => console.log('Launched navigator'),
                error => console.log('Error launching navigator: ' + error)
                );
            }
        });
            
    }
}
