import { Injectable, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard';



@Injectable()
export class PageNavigationProvider {

    private urlStack: string[] = [];
    constructor(        
        private router: Router,
        private keyboard:Keyboard,
        private zone: NgZone
    ) { }


    private redirect(url: string, params?: any[]): void {

        let navlink = [];
        navlink.push(url);

        if (params) {
            params.forEach((param) => {
                navlink.push(param);
            });
        }

        this.zone.run(() =>{
            this.keyboard.close();
            this.router.navigate(navlink);
        });       
    }


    navigate(url: string, params?: any[]): void {       
        this.urlStack.push(this.router.url);
        this.redirect(url,params);
    }  
    
    hasNavigation():boolean{
        return this.urlStack.length > 0;
    }

    return(): void {

        if (this.urlStack.length > 0) {
            this.zone.run(() => {
                this.router.navigate([this.urlStack.pop()]);
            });           
        }
    }

    reset(): void {
        this.urlStack = [];
    }

    replace(url: string, params?: any[]) {        

        this.redirect(url,params);

    }

}