import { ILocation } from "../models/iLocation";
import { XChangeProvider } from "./xchange.provider";
import { MapService } from "./map.provider"

export class RidesNearMe {
    private handle: any;
    private started: boolean = false;
    private isExit: boolean = false;

    constructor(private geo: ILocation, private map: MapService, private xchange: XChangeProvider) {

    }

    start(): void {
        if (this.started) {           
            return console.log("Tracking rides near me already started");
        }

        this.started = true;
        this.isExit = false;
        this.monitor(0);

        console.log("Ride near me started");
    }

    stop(): void {
        if(!this.started){
            return console.log("Tracking rides not started");
        }

        clearTimeout(this.handle);
        this.started = false;
        this.isExit = true;
        this.handle = null;

        console.log("Ride near me stopped");
    }

    private monitor(timeOut:number): void {
        var self = this;
        this.handle = setTimeout(() => {
            if (self.isExit) {
                return;
            }

            self.xchange.getRideNearMe(self.geo)
                .then(data => {
                    self.map.setRidesToLocation(data);
                })
                .then(()=>{
                    self.monitor(3000);
                })
                .catch(error =>{
                    console.error(error);
                    self.monitor(3000);
                })

        }, timeOut);

    }
}