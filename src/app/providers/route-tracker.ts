import { LocationTracker } from "./location-tracker.provider";
import { EventProvider } from "./event.provider";
import { ILocation } from "../models/iLocation";
import { MapService } from './map.provider';

export class RouteTracker {
    private started: boolean = false;
    private prevLocation: ILocation;
    private subscription: any;
    private tripId: string;   

    constructor(private location: LocationTracker, private event: EventProvider, private map:MapService, private type:number) {

    }

    


    start(tripId: string): void {
        if (this.started) {
            return console.log("Location tracking already running. Stop current tracking to enable new trip");
        }

        this.tripId = tripId;

        this.prevLocation = null;

        this.subscription = this.location.subscribeToLocationTracking()
            .subscribe((location: ILocation) => {
                this.setLocation(location);
            });


        this.location.startTracking();
        this.started= true;

        console.log("Routing tracking started");

    }

    stop(): void {
        if (!this.started) {
            return console.log("Route tracking not started");
        }

        this.prevLocation = null;
        this.location.stopTracking(this.subscription);

        this.started = false;

         console.log("Routing tracking stoppped");

    }


    private setLocation(current: ILocation): void {

        if (this.prevLocation &&
            this.prevLocation.latitude == current.latitude &&
            this.prevLocation.longitude == current.longitude) {
            return;
        }


        let data: any = { location: current, tripId: this.tripId};
        
        this.map.selfNavigation(current,this.type);
        this.event.trackCurrentLocation(data);
        this.prevLocation = current;
    }
}