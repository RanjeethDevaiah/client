import { Injectable } from '@angular/core';
declare var window:any;
@Injectable()
export class ScriptLoaderService {

    loadScript(url:string, callback:any): void {
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.onload = callback;
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }

}