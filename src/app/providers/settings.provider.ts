import { Injectable } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Network } from '@ionic-native/network';

import { Observable } from 'rxjs/Observable';

@Injectable()
export class SettingsProvider {

    constructor(private diagnostic: Diagnostic, private network: Network) { }


    checkIfLocationEnabled(): Promise<boolean> {
        return this.diagnostic.isLocationAvailable()
            .then(available => {
                if (available) return true;
                else throw "Location is not enabled";
            })
    }

    checkIfLocationAuthorized(): Promise<boolean> {
        return this.diagnostic.isLocationAuthorized()
            .then(available => {
                if (available) return available;
                else throw "Location is not authorized.";
            });
    }

    switchToGPS(): void {
        this.diagnostic.switchToLocationSettings();
    }

    requestLocationAuthorization(): Promise<any> {
        return this.diagnostic.requestLocationAuthorization();
    }

    checkConnection(): any {
        return this.network.type;
    }

    subscribeNetworkOnConnect(): Observable<any> {
        return this.network.onConnect();
    }

    subscribeNetworkDisConnect(): Observable<any> {
        return this.network.onDisconnect();
    }

}

