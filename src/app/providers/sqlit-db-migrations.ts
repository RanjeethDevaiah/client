import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

export class SqliteDbMigrations {

    private db: SQLiteObject;

    constructor(private sqlite: SQLite) { }

    getDb(): SQLiteObject {
        return this.db;
    }


    intitialize(): Promise<void> {
        return this.createDb()
            .then((db: SQLiteObject) => {
                this.db = db;
                console.log("Db created");
                return this.createTable("config", [
                    "key TEXT PRIMARY KEY",
                    "value TEXT NOT NULL"
                ]);
            })
            .then(() => {
                return this.createTable("way_points", [
                    "trip_id TEXT NOT NULL",
                    "location TEXT NOT NULL",
                    "updated DATETIME NOT NULL"
                ]);
            })
            .then(() => {
                 return this.createTable("trip_details", [
                    "trip_id TEXT NOT NULL PRIMARY KEY",
                    "customer TEXT NOT NULL",
                    "totalDistance INTEGER NOT NULL",
                    "totalWaitTime INTEGER NOT NULL",                   
                    "processed INTEGER NOT NULL DEFAULT 0",
                    "transaction_id NOT NULL",
                    "updated DATETIME NOT NULL",
                ]);
            })
            .then(() => {
                return this.createTable("transactions", [
                    "trip_id TEXT NOT NULL PRIMARY KEY",
                    "customer TEXT NOT NULL",
                    "amount REAL NOT NULL",
                    "transaction_id TEXT NOT NULL",
                    "updated DATETIME NOT NULL"
                ]);
            })
            .then(() => {
                return this.createTable("trip_complete", [
                    "trip_id TEXT NOT NULL PRIMARY KEY",
                    "driver TEXT NOT NULL",
                    "amount REAL NOT NULL",
                    "tip REAL",
                    "processed INTEGER NOT NULL DEFAULT 0",                  
                    "updated DATETIME NOT NULL"
                ]);
            })
            .then(() => {
                return this.createTable("trip_location", [
                    "trip_id TEXT NOT NULL",
                    "start_loc TEST NOT NULL",
                    "end_loc TEXT NOT NULL",  
                    "start_address TEXT NULL",
                    "end_address TEXT NULL",    
                    "start_geo TEXT NULL", 
                    "end_geo TEXT NULL",                     
                    "updated DATETIME NOT NULL"
                ]);
            })
            .then(() => {
                return this.createTable("favourites", [
                    "trip_id TEXT NOT NULL PRIMARY KEY",
                    "end_loc REAL NOT NULL",                      
                    "end_address TEXT NULL", 
                    "end_geo TEXT NULL",                                 
                    "updated DATETIME NOT NULL",
                    "type INTEGER NOT NULL"
                ]);
            })
            .then(() => {
                return this.createTable("version", [
                    "version REAL NOT NULL PRIMARY KEY"                    
                ]);
            });
    }

    private createDb(): Promise<any> {
        console.log("Migration intialized");
        return this.sqlite.create({ name: "cab.db", location: "default" });
    }

    private createTable(tableName: string, parameters: any[]): Promise<void> {
        return this.db.executeSql(`CREATE TABLE IF NOT EXISTS ${tableName} (${parameters.join()})`, {})
            .then((data) => {
                console.log(`Table [ ${tableName} ]`);
            })
            .catch((error) => {
                throw `Error creating table ${tableName}, ${error}`;
            })
    }

    private executeStatement(statement: string): Promise<void> {      
        return this.db.executeSql(statement, []);
    }

   upgrade(): Promise<void> {
        return Promise.resolve();
        //return this.executeStatement("ALTER TABLE favourites ADD type INTEGER NOT NULL DEFAULT 0");
    }

    
}