import { SQLiteObject } from '@ionic-native/sqlite';
import { XChangeProvider } from './xchange.provider';
import { LocationTracker } from "./location-tracker.provider";
import { ILocation } from "../models/iLocation";
import { MapService } from './map.provider';
import { EventProvider } from "./event.provider";

export class UploadWayPoints {
    private tripId: string;
    private isExit: boolean;
    private startIndex: number;
    private publishCount: number = 100;
    private promise: Promise<void>;
    private started: boolean = false;
    private timeOutInterval: number = 4000;
    private subscription: any;

    constructor(private db: SQLiteObject, private xchange: XChangeProvider,
        private location: LocationTracker, private map: MapService, private event: EventProvider, private type) {

    }

    private processWayPoints(index: number, resolve: Function, timeOut: number): void {
        var self = this;
        console.log(`Processing waypoints time out set ${timeOut}.`);
        setTimeout(() => {
            let dataCount: number;

            self.getWayPoints(self.tripId, index, self.publishCount)
                .then((results: [any]) => {
                    dataCount = results.length;

                    if (dataCount) {
                        console.log(`Processing waypoints value from ${index} upto ${dataCount}.`);
                        //return self.xchange.addWayPoints(self.tripId, results, index);

                        return self.event.trackRoute({
                            tripId: this.tripId,
                            waypoints: results
                        });
                    }
                    else {
                        return;
                    }
                })
                .then(() => {
                    if (dataCount) {
                        if (dataCount < self.publishCount) {
                            if (self.isExit)
                                self.processWayPoints(index + dataCount, resolve, 0);
                            else
                                self.processWayPoints(index + dataCount, resolve, self.timeOutInterval);
                        }
                        else {
                            self.processWayPoints(index + dataCount, resolve, 0);
                        }
                    }
                    else {
                        if (self.isExit) {
                            self.started = false;
                            resolve();
                        }
                        else {
                            self.processWayPoints(index + dataCount, resolve, self.timeOutInterval);
                        }
                    }
                })
                .catch(error => {
                    console.error(error);
                    self.processWayPoints(index, resolve, 1000);
                })

        }, timeOut);
    }

    start(tripId: string, count?: number): void {

        if (this.started) {
            console.log(`Way Point process running for tripId ${tripId}`);
            return;
        }

        this.tripId = tripId;
        this.startIndex = 0;
        this.isExit = false;
        if (count) {
            this.publishCount = count;
        }

        this.subscription = this.location.subscribeToLocationTracking()
            .subscribe((location: ILocation) => {              
                this.map.addLocationToDistance(this.tripId, location, Date.now())
                    .then(() => {
                        this.map.selfNavigation(location, this.type);
                    })
                    .catch(error => {
                        console.error("Error updating waypoint " + error);
                    });

            }, error => console.error(error));

        this.location.startTracking();

        this.promise = new Promise<void>((resolve, reject) => {
            this.processWayPoints(0, resolve, 0);
        });

        this.started = true;
    }

    stop(): Promise<void> {
        if (!this.started)
            Promise.resolve();

        this.location.stopTracking(this.subscription);
        this.isExit = true;
        return this.promise;
    }

    private getWayPoints(tripId: string, from: number, count: number): Promise<[any]> {

        return this.db.executeSql("SELECT location FROM way_points WHERE trip_id = ? ORDER BY updated LIMIT ? OFFSET ?", [tripId, count, from])
            .then((results: any) => {
                let wayPoints: any = [];

                for (var i = 0; i < results.rows.length; i++) {
                    wayPoints.push(JSON.parse(results.rows.item(0).location));
                }
                return wayPoints;
            });
    }
}