import { Injectable } from '@angular/core';
import { IPlace } from '../models/IPlace';
import { IProximity } from '../models/iProximity';
import { ITypeLocation } from '../models/iTypeLocation';
import { IRegistration, IProfile } from '../models/iRegistration';
import { ILocation } from '../models/iLocation';
import { StateProvider } from './app-state.provider';
import { HttpClientProvider } from './httpClient.provider';

import 'rxjs/add/operator/toPromise';

//'http://67.161.49.168/api'

@Injectable()
export class XChangeProvider {
    constructor(private appState: StateProvider, private httpClient: HttpClientProvider) {
    }

    registerProfile(profile: IProfile): Promise<any> {
        return this.httpClient.post("customer_regis/profile", {
            "firstName": profile.first_name,
            "lastName": profile.last_name,
            "email": profile.email,
            "phone": profile.phone,
            "image": profile.image
        });
    }

    registerToQueue(location: ILocation): Promise<boolean> {
        let data: any = { location: location };
        return this.appState.getDriverId()
            .then(id => {
                data.id = id;
                data.updateDate = new Date().getTime();
                return this.appState.getType();
            })
            .then(type => {
                data.type = type;
                return this.httpClient.post(`ridequeue/queue`, data);
            })
            .then(data => {
                return true;
            })

    }

    getRideNearMe(location: ILocation): Promise<ITypeLocation> {
        return this.httpClient.get(`ridequeue/queue/${location.latitude}/${location.longitude}`);
    }

    exitWaiting(): Promise<boolean> {
        return this.appState.getDriverId()
            .then(id => {
                return this.httpClient.delete(`ridequeue/queue/${id}`);
            })
            .then(data => { return true });

    }

    seekRide(start: IPlace, end: IPlace, distance: number, type: number): Promise<any> {
        let data: any = {}
        data.start = start;
        data.type = type;
        return this.appState.getGuestId()
            .then(id => {
                data.id = id;
                if (end)
                    data.end = end;
                if (distance)
                    data.approxDistance = distance;
                return this.httpClient.post(`customerRequest/queue`, data);
            });
    }

    requestForPooledDrivers(tripId: any, type: any): Promise<any> {
        return this.appState.getGuestId()
            .then(id => {
                return this.httpClient.get(`customerRequest/proximity/${id}/${tripId}/${type}`);
            })
            .then((data) => {
                return data;
            });
    }
    updateCustomer(registration: IRegistration): Promise<any> {
        return this.httpClient.put(`customer_regis/details`, registration);
    }

    registerCustomer(registration: IRegistration): Promise<any> {
        return this.httpClient.post(`customer_regis/details`, registration);
    }

    getClientAuth(): Promise<string> {
        return this.httpClient.get(`customer_regis/clientToken`);
    }

    createClientPayMethod(token: string) {
        let data: any = { "token": token };

        return this.appState.getGuestId()
            .then(id => {
                data.customerId = id;
                return this.httpClient.post(`customer_regis/paymentMode`, data);
            })
    }


    acceptedRideRequest(customerId, tripId): Promise<any> {
        let data: any = {
            customer: customerId,
            tripId: tripId
        };

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id
                return this.httpClient.put('ridequeue/accept_ride', data);
            });
    }

    tripStarted(customerId, tripId): Promise<any> {
        let data: any = {
            customer: customerId,
            tripId: tripId
        }

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put('ridequeue/trip_started', data);
            });
    }

    reachedLocation(customerId, tripId, location: ILocation): Promise<any> {
        let data: any = {
            customer: customerId,
            tripId: tripId,
            location: location
        };

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put('ridequeue/rchd_loc', data);
            })
    }

    tripComplete(customerId, tripId, totalDistance: Number, waitTime: number) {
        let data: any = {
            customer: customerId,
            tripId: tripId,
            totalDistance: totalDistance,
            waitTime: waitTime
        };

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put('ridequeue/cmplt_trip', data);
            });
    }

    tripCanceledDriver(customerId, tripId, status): Promise<any> {
        let data: any = {
            customer: customerId,
            tripId: tripId,
            status: status
        };

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put('ridequeue/driver_cancel', data);
            });
    }

    tripCanceledCustomer(driverId, tripId): Promise<any> {
        let data: any = {
            tripId: tripId,
            driver: driverId
        };

        return this.appState.getGuestId()
            .then(id => {
                data.customer = id;
                return this.httpClient.put('ridequeue/customer_cancel', data);
            });
    }

    ignorePendingRequets(customer, tripId): Promise<any> {
        let data: any = {
            customer: customer,
            tripId: tripId
        };


        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put('ridequeue/pending_request', data);
            });
    }

    proceedToLocation(tripId, driverId): Promise<any> {
        let data: any = {
            tripId: tripId,
            driver: driverId
        };

        return this.appState.getGuestId()
            .then(id => {
                data.customer = id;
                return this.httpClient.put('ridequeue/proc_loc', data);
            });
    }

    rejectRide(): Promise<any> {
        return this.appState.getDriverId()
            .then(id => {
                let data = { driver: id };
                return this.httpClient.put('ridequeue/reject_req', data);
            });
    }

    checkToProceed(customerId, tripId): Promise<any> {
        return this.httpClient.get(`ridequeue/check2proceed/${customerId}/${tripId}`);
    }

    addAccountDetails(details: any): Promise<any> {
        return this.httpClient.put('customer_regis/acc_details', details);
    }

    addWayPoints(tripId: string, wayPoints: [any], startIndex: number): Promise<any> {
        let data: any = {
            tripId: tripId,
            wayPoints: wayPoints,
            startIndex: startIndex
        }

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.post('ridequeue/waypoints', data);
            });

    }

    requestPayment(data: any): Promise<any> {

        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.post('payment/pay', data);
            });
    }

    requestTipsPayment(driverId: string, tripId: string, tipAmount: number) {
        let data: any = {
            driver: driverId,
            tripId: tripId,
            tip: tipAmount
        };
        return this.appState.getGuestId()
            .then(id => {
                data.customer = id;
                return this.httpClient.put("payment/tips", data);
            });
    }

    sendDriverFeedback(driverId: string, tripId: string, rating: number, dontRecommend: boolean) {

        let data: any = {
            driver: driverId,
            rating: rating,
            dontRecommend: dontRecommend,
            tripId: tripId
        };
        return this.appState.getGuestId()
            .then(id => {
                data.customer = id;
                return this.httpClient.put("feedback/driver", data);
            });

    }

    sendCustomerFeedback(customerId: string, dontRecommend: boolean) {

        let data: any = {
            customer: customerId,
            dontRecommend: dontRecommend
        };
        return this.appState.getDriverId()
            .then(id => {
                data.driver = id;
                return this.httpClient.put("feedback/customer", data);
            });

    }

    setToken(): Promise<any> {
        let data: any = {}
        let promise = (this.appState.isDriverMode()) ? this.appState.getDriverId()
            : this.appState.getGuestId();

        return promise
            .then((id: any) => {
                data.id = id;
                return (this.appState.isDriverMode()) ? this.appState.getDriverProfile()
                    : this.appState.getGuestProfile();
            })
            .then((profile: any) => {
                data.secret = profile.secret;
                return this.httpClient.post("security/auth", data)
            })
            .then((token: any) => {
                this.appState.setToken(token)
            });
    }


    getApproximate(start: ILocation, end: ILocation, city: string) {
        return this.httpClient.get(`customerrequest/approximation?start=${start.latitude},${start.longitude}&end=${end.latitude},${end.longitude}&city=${city}`);
    }

    sendAuthToken(authCode: number, isDriver: boolean):Promise<any> {
        var data: any = {
            code: authCode
        }

        var promise = (isDriver) ? this.appState.getDriverId() : this.appState.getGuestId();

        return promise.then((id) => {
            data.id = id;

            return this.httpClient.put("customer_regis/authCode", data);
        })



    }

    validateAuthToken(authToken: number, isDriver: boolean):Promise<any> {

        var promise = (isDriver) ? this.appState.getDriverId() : this.appState.getGuestId();

        return promise.then((id) => {           
            return this.httpClient.get(`customer_regis/authCode/${id}/${authToken}`);
        })
       
    }
}