var CustomMarker = function (path, scale, callback) {
    this.canvas = document.createElement('canvas');    
    this.image = new Image();
    this.image.src = path;
    
    var callback = callback;
    var self = this;
    this.image.onload = function () {
        self.canvas.width = scale;
        self.canvas.height = scale;
        var context = self.canvas.getContext('2d');
        context.save();
        context.drawImage(self.image, 0, 0, self.canvas.width, self.canvas.height);
        self.image = document.createElement('canvas');
        var imgCtx = self.image.getContext('2d');
        imgCtx.imageSmoothingEnabled = false;
        imgCtx.drawImage(self.canvas, 0, 0, scale, scale);
        context.restore();
        if (callback)
            callback();
    }
}
CustomMarker.prototype.load = function (scale) {
    this.canvas.width = scale;
    this.canvas.height = scale;
    var context = this.canvas.getContext('2d');
    context.save();
    context.drawImage(this.image, 0, 0, this.canvas.width, this.canvas.height);    
    this.image = document.createElement('canvas');
    var imgCtx = this.image.getContext('2d');   
    imgCtx.drawImage(this.canvas, 0, 0, scale, scale);    
    context.restore();
}
CustomMarker.prototype.rotate = function (angle) {    
    var context = this.canvas.getContext('2d');
    context.save();
    context.clearRect(0, 0, this.canvas.width, this.canvas.height);
    context.translate(0, 0);
    context.translate(this.canvas.width / 2, this.canvas.height/2);
    context.rotate(Math.round(angle) * Math.PI / 180);
    context.drawImage(this.image, -(this.canvas.width / 2), -(this.canvas.height / 2));
    context.restore();
    return this.canvas.toDataURL('image/png');
}

CustomMarker.prototype.sharpen = function (mix) {
    var ctx = this.image.getContext('2d'),
        w = this.image.width, h = this.image.height;
    var weights = [0, -1, 0, -1, 5, -1, 0, -1, 0],
        katet = Math.round(Math.sqrt(weights.length)),
        half = (katet * 0.5) | 0,
        dstData = ctx.createImageData(w, h),
        dstBuff = dstData.data,
        srcBuff = ctx.getImageData(0, 0, w, h).data,
        y = h;

    while (y--) {

        x = w;

        while (x--) {

            var sy = y,
                sx = x,
                dstOff = (y * w + x) * 4,
                r = 0, g = 0, b = 0, a = 0;

            for (var cy = 0; cy < katet; cy++) {
                for (var cx = 0; cx < katet; cx++) {

                    var scy = sy + cy - half;
                    var scx = sx + cx - half;

                    if (scy >= 0 && scy < h && scx >= 0 && scx < w) {

                        var srcOff = (scy * w + scx) * 4;
                        var wt = weights[cy * katet + cx];

                        r += srcBuff[srcOff] * wt;
                        g += srcBuff[srcOff + 1] * wt;
                        b += srcBuff[srcOff + 2] * wt;
                        a += srcBuff[srcOff + 3] * wt;
                    }
                }
            }

            dstBuff[dstOff] = r * mix + srcBuff[dstOff] * (1 - mix);
            dstBuff[dstOff + 1] = g * mix + srcBuff[dstOff + 1] * (1 - mix);
            dstBuff[dstOff + 2] = b * mix + srcBuff[dstOff + 2] * (1 - mix)
            dstBuff[dstOff + 3] = srcBuff[dstOff + 3];
        }
    }

    ctx.putImageData(dstData, 0, 0);
}