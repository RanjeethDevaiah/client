// JavaScript source code

function Maps() {
    this.labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    this.customMarkerScale = 32;
    this.state = {};
    this.deltaAngle = 30;
    this.state.current = {};
    this.state.end = {};
    this.markers = [];
    var _init = false;
    this.placeChangedSub = new Rx.Subject();
    this.mapClickedSub = new Rx.Subject();
    this.currentLocationChanged = new Rx.Subject();
    this.autoComplete = {};
    this.self = {};

    this.init = function (canvas) {
        if (_init)
            return;

        var styles = [{
            "stylers": [{
                "hue": "#008eff"
            }]
        },
        {
            "featureType": "poi",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "road",
            "stylers": [{
                "saturation": "0"
            },
            {
                "lightness": "0"
            }
            ]
        },
        {
            "featureType": "road",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#648cc8"
            }]
        },
        {
            "featureType": "road.highway",
            "elementType": "geometry.stroke",
            "stylers": [{
                "color": "#004080"
            }]
        },
        {
            "featureType": "transit",
            "stylers": [{
                "visibility": "off"
            }]
        },
        {
            "featureType": "water",
            "stylers": [{
                "saturation": "-60"
            },
            {
                "lightness": "-20"
            },
            {
                "visibility": "simplified"
            }
            ]
        }
        ];

        var lineSymbol = {
            path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
            strokeOpacity: 1,
            fillOpacity: 0.7,
            scale: 3,
            strokeWeight: 1
        };

        this.dot = new CustomMarker('img/dot.png', this.customMarkerScale, function () { });

        this.taxiMarker = new CustomMarker('img/taxi.png', this.customMarkerScale, function () { });

        this.shareMarker = new CustomMarker('img/share.png', this.customMarkerScale, function () { });

        this.directionsService = new google.maps.DirectionsService;
        this.geocoder = new google.maps.Geocoder;
        this.distanceMatrix = new google.maps.DistanceMatrixService;
        var prop = {
            mapTypeControl: false,
            //     center: new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude),
            zoom: 16,
            maxZoom: 16,
            minZoom:5,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            streetViewControl: false,
            zoomControl: false,
            zoomControlOptions: { position: google.maps.ControlPosition.RIGHT_CENTER },
            styles: styles
        };
        this.map = new google.maps.Map(canvas, prop);
        /*var trafficLayer = new google.maps.TrafficLayer();
        trafficLayer.setMap(this.map);*/
        var self = this;
        _init = true;

        this.map.addListener('click', function (event) {

            self.mapClickedSub.onNext(event.latLng);
        });
    }
}

Maps.prototype.onPlaceChanged = function () {
    return this.placeChangedSub;
}

Maps.prototype.setFromSeachBox = function (searchElement) {

    if (this.autoComplete.from) {
        google.maps.event.removeListener(this.autoComplete.from.listner);
        google.maps.event.clearInstanceListeners(this.autoComplete.from.autocomplete);
        this.autoComplete.from.autocomplete.unbindAll();
    } else {
        this.autoComplete.from = {};
    }

    var autocomplete = new google.maps.places.Autocomplete(searchElement);
    this.autoComplete.from.autocomplete = autocomplete;

    autocomplete.bindTo('bounds', this.map);
    var self = this;

    this.autoComplete.from.listner = autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        if (!place.geometry) {
            self.placeChangedSub.onError({ location: {}, address: place.name });
            return;
        }

        var location = { latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() };

        self.setCurrentLocation(location, place);

        self.placeChangedSub.onNext({ location: location, address: place });

    });
}

Maps.prototype.setToSeachBox2 = function (searchElement) {

    if (this.autoComplete.to2) {
        google.maps.event.removeListener(this.autoComplete.to2.listner);
        google.maps.event.clearInstanceListeners(this.autoComplete.to2.autocomplete);
        this.autoComplete.to2.autocomplete.unbindAll();
    } else {
        this.autoComplete.to2 = {};
    }

    var autocomplete = new google.maps.places.Autocomplete(searchElement);
    this.autoComplete.to2.autocomplete = autocomplete;
    autocomplete.bindTo('bounds', this.map);
    var self = this;

    this.autoComplete.to2.listner = autocomplete.addListener('place_changed', function () {

        var place = autocomplete.getPlace();
        if (!place.geometry) {
            self.placeChangedSub.onError({ location: {}, address: place.name });
            return;
        }
        var location = { latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() };
        self.setEndLocation(location, place);
        self.placeChangedSub.onNext({ location: location, address: place });

    });
}

Maps.prototype.setToSeachBox = function (searchElement) {

    if (this.autoComplete.to) {
        google.maps.event.removeListener(this.autoComplete.to.listner);
        google.maps.event.clearInstanceListeners(this.autoComplete.to.autocomplete);
        this.autoComplete.to.autocomplete.unbindAll();
    } else {
        this.autoComplete.to = {};
    }

    var autocomplete = new google.maps.places.Autocomplete(searchElement);
    this.autoComplete.to.autocomplete = autocomplete;

    autocomplete.bindTo('bounds', this.map);
    var self = this;

    this.autoComplete.to.listner = autocomplete.addListener('place_changed', function () {

        var place = autocomplete.getPlace();
        if (!place.geometry) {
            self.placeChangedSub.onError({ location: {}, address: place.name });
            return;
        }
        var location = { latitude: place.geometry.location.lat(), longitude: place.geometry.location.lng() };
        self.setEndLocation(location, place);
        self.placeChangedSub.onNext({ location: location, address: place });

    });
}

Maps.prototype.adjustView = function () {

    if (!this.state.end.latitude && !this.state.end.longitude) {
        this.map.setCenter(new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude));
        return;
    }

    var currentBound = this.map.getBounds() ? this.map.getBounds() : new google.maps.LatLngBounds();

    if (currentBound.contains(new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude)) &&
        currentBound.contains(new google.maps.LatLng(this.state.end.latitude, this.state.end.longitude))) {

        this.map.setCenter(currentBound.getCenter());
        this.map.fitBounds(currentBound);
        return;
    }

    if (!currentBound.contains(new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude)))
        currentBound.extend(new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude));

    if (!currentBound.contains(new google.maps.LatLng(this.state.end.latitude, this.state.end.longitude)))
        currentBound.extend(new google.maps.LatLng(this.state.end.latitude, this.state.end.longitude));

    this.map.setCenter(currentBound.getCenter());
    this.map.fitBounds(currentBound);
}

Maps.prototype.clearLocationMarkers = function () {
    if (this.state.current && this.state.current.marker) {
        this.state.current.marker.setMap(null);
        this.state.current.marker = null;
    }

    if (this.state.end && this.state.end.marker) {
        this.state.end.marker.setMap(null);
        this.state.end.marker = null;
    }
}

Maps.prototype.setCurrentLocation = function (location, address, ignoreMarker) {
    this.state.current.latitude = location.latitude;
    this.state.current.longitude = location.longitude;

    if (address) {
        this.state.current.address = address;
    } else {
        this.reverseGeocoding(this.state.current)
            .then(function (address) {
                self.state.current.address = address;
            }).catch(function (error) { reject(error) });
    }

    if (this.state.current.marker) {
        google.maps.event.clearListeners(this.state.current.marker, 'click');
        this.state.current.marker.setMap(null);
        delete this.state.current.marker;
    }

    if (!ignoreMarker) {
        this.state.current.marker = this.createMarker(this.state.current, 'Start', 'img/start.png', false, null);
        this.state.current.marker.setMap(this.map);
    }
    var self = this;
    this.adjustView();

    this.currentLocationChanged.onNext(location);
}

Maps.prototype.subscribeCurrentLocationChange = function () {
    return this.currentLocationChanged;
}

Maps.prototype.setMarkerOverlay = function (element) {
    var CustomMarker = MarkerFactory();
    this.state.overlay = new CustomMarker(this.map, new google.maps.LatLng(this.state.current.latitude, this.state.current.longitude), element);
}

Maps.prototype.removeMarkerOverlay = function (element) {
    if (this.state.overlay) {
        this.state.overlay.remove();
        this.state.overlay = null;
    }

}

Maps.prototype.setEndLocation = function (location, address) {
    this.state.end.latitude = location.latitude;
    this.state.end.longitude = location.longitude;

    if (address) {
        this.state.end.address = address;
    } else {
        this.reverseGeocoding(this.state.end)
            .then(function (address) {
                self.state.end.address = address;
            }).catch(function (error) { reject(error) });
    }

    if (this.state.end.marker) {
        google.maps.event.clearListeners(this.state.end.marker, 'click');
        this.state.end.marker.setMap(null);
        delete this.state.end.marker;
    }

    this.state.end.marker = this.createMarker(this.state.end, 'Stop', 'img/stop.png', false, null);
    this.state.end.marker.setMap(this.map);
    var self = this;
    this.adjustView();
}

Maps.prototype.approxTotalDistance = function () {
    var self = this;
    return new Promise(function (resolve, reject) {
        var start = self.state.current.latitude + ', ' + self.state.current.longitude;
        var end = self.state.end.latitude + ', ' + self.state.end.longitude;
        self.state.totalDistance = 0;
        self.directionsService.route({
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.DRIVING
        }, function (response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                self.state.approxDistance = response.routes[0].legs[0].distance.value;
                resolve(self.state.approxDistance);
            } else {
                reject(status);
            }
        });
    });
}

Maps.prototype.initializeDistanceCaluclation = function () {
    this.state.totalDistance = 0;
    this.state.prevDistLocation = null;
}

Maps.prototype.nextTraveledLocation = function (nextLocation) {
    if (this.state.prevDistLocation) {
        this.state.totalDistance += this.getDistanceFromLatLon(
            this.state.prevDistLocation.latitude,
            this.state.prevDistLocation.longitude,
            nextLocation.latitude,
            nextLocation.longitude
        );
    }
    this.state.prevDistLocation = nextLocation;
}

Maps.prototype.getTotalDistance = function () {
    return this.state.totalDistance;
}

Maps.prototype.createEmbededIcon = function (heading, embeddedMarker) {

    var embedded = embeddedMarker.rotate(heading);
    var icon = {
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(this.customMarkerScale / 2, this.customMarkerScale / 2),
        url: embedded,

    }

    return icon;
}

Maps.prototype.computeHeading = function (prev, current) {
    var heading = prev ? Math.round(google.maps.geometry.spherical.computeHeading(prev, current)) : 0;
    return (heading > 0) ? heading : 360 + heading;
}

Maps.prototype.setNavMarker = function (prev, current, marker, prevHeading, embeddedMarker, location) {
    if (prev && prev.equals(current))
        return prevHeading;

    var absHeading = this.computeHeading(this.state.prevLatLng, location);

    if (prevHeading != absHeading) {
        var icon = this.createEmbededIcon(absHeading, embeddedMarker);
        marker.setIcon(icon);
    }

    marker.setPosition(location);
    this.state.prevLatLng = location;

    return absHeading;
}

Maps.prototype.setNavHeading = function (loc, type) {
    var location = new google.maps.LatLng(loc.latitude, loc.longitude);

    if (!this.state.nav_marker) {
        this.state.heading = 0;
        var icon;

        if (type === 1) {
            icon = this.createEmbededIcon(this.state.heading, this.taxiMarker);
        } else if (type === 2) {
            icon = this.createEmbededIcon(this.state.heading, this.shareMarker);
        } else {
            icon = this.createEmbededIcon(this.state.heading, this.dot);
        }

        this.state.nav_marker = this.createMarker(loc, "Current", null, false, null, icon);
        this.state.nav_marker.setMap(this.map);
        this.state.prevLatLng = location;
    } else {

        if (type === 1) {
            this.state.heading = this.setNavMarker(this.state.prevLatLng, location, this.state.nav_marker, this.state.heading, this.taxiMarker, location);
        } else if (type === 2) {
            this.state.heading = this.setNavMarker(this.state.prevLatLng, location, this.state.nav_marker, this.state.heading, this.shareMarker, location);
        } else {
            this.state.heading = this.setNavMarker(this.state.prevLatLng, location, this.state.nav_marker, this.state.heading, this.dot, location);
        }
    }

    var self = this;


}

Maps.prototype.setNavMarkerSelf = function (prev, current, marker, prevHeading, embeddedMarker, location) {
    if (prev && prev.equals(current))
        return prevHeading;

    var absHeading = this.computeHeading(this.self.prevLatLng, location);

    if (prevHeading != absHeading) {
        var icon = this.createEmbededIcon(absHeading, embeddedMarker);
        marker.setIcon(icon);
    }

    marker.setPosition(location);
    this.self.prevLatLng = location;
    return absHeading;
}

Maps.prototype.setNavHeadingSelf = function (loc, type) {
    var location = new google.maps.LatLng(loc.latitude, loc.longitude);

    this.self.type = type;

    if (!this.self.nav_marker) {
        if (!this.heading) {
            this.heading = 0;
        }

        var icon;

        if (type === 1) {
            icon = this.createEmbededIcon(this.self.heading, this.taxiMarker);
        } else if (type === 2) {
            icon = this.createEmbededIcon(this.self.heading, this.shareMarker);
        } else {
            icon = this.createEmbededIcon(this.self.heading, this.dot);
        }

        this.self.nav_marker = this.createMarker(loc, "Current", null, false, null, icon);
        this.self.nav_marker.setMap(this.map);
        this.self.prevLatLng = location;
    } else {

        if (type === 1) {
            this.self.heading = this.setNavMarkerSelf(this.self.prevLatLng, location, this.self.nav_marker, this.self.heading, this.taxiMarker, location);
        } else if (type === 2) {
            this.self.heading = this.setNavMarkerSelf(this.self.prevLatLng, location, this.self.nav_marker, this.self.heading, this.shareMarker, location);
        } else {
            this.self.heading = this.setNavMarkerSelf(this.self.prevLatLng, location, this.self.nav_marker, this.self.heading, this.dot, location);
        }
    }
}

Maps.prototype.resetToInitial = function (location, ignoreMarker) {

    if (this.state.nav_marker) {
        this.state.nav_marker.setMap(null);
        this.state.nav_marker = null;
    }

    if (this.self.nav_marker) {
        this.self.nav_marker.setMap(null);
        this.self.nav_marker = null;
    }

    this.clearLocationMarkers();

    this.state = {};
    this.state.current = {};
    this.state.end = {};
    this.self = {};

    this.setCurrentLocation(location, null, ignoreMarker);
    var loc = new google.maps.LatLng(location.latitude, location.longitude);

    this.map.panTo(loc);

    this.map.setZoom(16);
}

Maps.prototype.resetNavHeading = function () {
    this.state.nav_marker.setMap(null);
    delete this.state.nav_marker;
    delete this.state.prevLatLng;
    delete this.state.heading;
}
Maps.prototype.subscribeOnMapClicked = function () {

    return this.mapClickedSub;
}

Maps.prototype.createMarker = function (location, name, path, draggable, label, icon) {

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(location.latitude, location.longitude),
        title: name,
        icon: (icon) ? icon : path,
        draggable: (draggable) ? true : false,
        label: label,
    });
    return marker;
}

Maps.prototype.getDistanceFromLatLon = function (lat1, lon1, lat2, lon2) {

    function deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2 - lat1); // deg2rad below
    var dLon = deg2rad(lon2 - lon1);
    var a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c; // Distance in km
    return d * 1000;
}
Maps.prototype.reverseGeocoding = function (location) {
    var self = this;
    return new Promise(function (resolve, reject) {
        var latlng = { lat: location.latitude, lng: location.longitude };
        self.geocoder.geocode({ 'location': latlng }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
                if (results[0])
                    resolve(results[0]);
                else
                    reject("No address");
            } else
                reject('Geocoder failed due to: ' + status);
        });
    });
}

Maps.prototype.getDistanceMatrix = function (start, end) {
    var self = this;
    return new Promise(function (resolve, reject) {

        self.distanceMatrix.getDistanceMatrix({
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false,
            origins: [{ lat: start.latitude, lng: start.longitude }],
            destinations: [{ lat: end.latitude, lng: end.longitude }],
        }, function (results, status) {
            if (status != 'OK') {
                reject(status);
            } else {
                var result = results.rows[0].elements[0];

                resolve({
                    text: {
                        distance: result.distance.text,
                        duration: result.duration.text
                    },
                    value: {
                        distance: result.distance.value,
                        duration: result.duration.value
                    }
                });
            }
        });

    });
}


Maps.prototype.setRandomHeadingMarker = function (typePositions) {
    var self = this;
    if (!this.tempMarkers) {
        this.tempMarkers = {};
    }


    typePositions.forEach(function (typePosition) {

        var current = new google.maps.LatLng(typePosition.location.latitude, typePosition.location.longitude);

        if (self.tempMarkers[typePosition.id]) {
            var tempMarker = self.tempMarkers[typePosition.id];
            var embeddedMarker = (typePosition.type == 1) ? self.taxiMarker : self.shareMarker;
            tempMarker.heading = self.setNavMarker(tempMarker.prevLocation, current, tempMarker.marker, tempMarker.heading, embeddedMarker, current);
            tempMarker.prevLocation = current;
            tempMarker.mark = 1;

        } else {
            var tempMarker = {};
            tempMarker.mark = 1;
            tempMarker.heading = self.getRandomInt(0, 360);
            var icon = (typePosition.type == 1) ? self.createEmbededIcon(tempMarker.heading, self.taxiMarker) : self.createEmbededIcon(tempMarker.heading, self.shareMarker);
            tempMarker.prevLocation = current;
            tempMarker.marker = self.createMarker(typePosition.location, "radom", null, false, null, icon);
            tempMarker.marker.setMap(self.map);
            self.tempMarkers[typePosition.id] = tempMarker;
        }
    });

    if (this.tempMarkers) {
        for (var property in this.tempMarkers) {
            if (this.tempMarkers.hasOwnProperty(property)) {
                if (this.tempMarkers[property].mark)
                    this.tempMarkers[property].mark = 0;
                else
                    delete this.tempMarkers[property];
            }
        }
    }
}

Maps.prototype.getRandomInt = function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}


Maps.prototype.clearRandomMarkers = function () {
    if (this.tempMarkers) {
        for (var property in this.tempMarkers) {
            if (this.tempMarkers.hasOwnProperty(property)) {
                this.tempMarkers[property].marker.setMap(null);
                delete this.tempMarkers[property];
            }
        }
    }
}

Maps.prototype.setStart2Center = function () {
    this.map.setCenter(this.state.current.marker.getPosition());
}

Maps.prototype.calculateSpeed = function (lat1, lng1,lat2, lng2, timeinSec) {
    // Distance in Metres
    var distance = this.getDistanceFromLatLon(lat1, lng1, lat2, lng2);
    var speed_mps = distance / timeinSec;

    var speed_kph = (speed_mps * 3600) / 1000;

    return speed_kph;

}

function MarkerFactory() {
    //http://humaan.com/custom-html-markers-google-maps/
    function CustomOverlayMarker(map, latlng, parentElement) {
        this.latlng = latlng;
        this.div = parentElement;
        this.setMap(map);
    }

    CustomOverlayMarker.prototype = new google.maps.OverlayView();

    CustomOverlayMarker.prototype.draw = function () {
        if (!this.div)
            return;

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
        this.div.style.position = 'absolute';
        var widthStr = getComputedStyle(this.div, null).getPropertyValue('width');
        var width = parseFloat(widthStr);
        this.div.style.left = point.x - Math.round(width / 2) + 'px';
        this.div.style.top = point.y - 78 + 'px';
        this.div.style.visibility = 'visible';
        var panes = this.getPanes();
        panes.overlayImage.appendChild(this.div);

    }

    CustomOverlayMarker.prototype.remove = function () {
        if (this.div) {
            this.div.style.visibility = 'visible';
            this.div.parentNode.removeChild(this.div);
            this.div = null;
        }
    }

    CustomMarker.prototype.getPosition = function () {
        return this.latlng;
    };

    return CustomOverlayMarker;
}